<?php
/**
 * The template for displaying Tag Archive pages.
 */

get_header(); ?>

				<section class="content content-main">
					<div class="inner">
						<h1 class="section-title"><?php printf( __( 'Tag Archives: %s', 'boilerplate' ), '' . single_tag_title( '', false ) . '' ); ?></h1>
						<?php get_template_part( 'loop', 'tag' ); ?>

						<?php get_sidebar(); ?>
					</div><!-- .inner -->
				</section><!-- .content-main -->

				<?php hk_paginate() ?>

<?php get_footer(); ?>