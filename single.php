<?php
/**
 * The Template for displaying all single posts.
 */

get_header(); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
				<section class="content content-main">
					<div class="inner">
						<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
							<?php if( has_post_thumbnail( $post_id ) ) : ?>
							<header>
								<div class="entry-thumb"><a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__( 'Permalink to %s', 'boilerplate' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark"><?php echo the_post_thumbnail('featured-image') ?></a></div>
								<div class="entry-meta">
									<?php boilerplate_posted_on(); ?>
								</div><!-- .entry-meta -->
							</header>
							<?php endif; ?>

							<h1 class="entry-title"><?php the_title(); ?></h1>

							<div class="entry-content">
								<?php the_content(); ?>
								<?php edit_post_link( __( 'Edit', 'boilerplate' ), '', '' ); ?>
							</div><!-- .entry-content -->

							<footer>
								<ul class="menu share">
									<li class="menu-item facebook"><a id="facebook-share" href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" target="_blank">Share This</a></li>
									<li class="menu-item twitter"><a id="twitter-share" href="http://twitter.com/home?status=Currently reading <?php the_permalink(); ?> via @HardKnocks" title="Click to send this page to Twitter!" target="_blank">Tweet This</a></li>
									<!-- <li class="menu-item google"><a id="google-share" href="https://plus.google.com/share?url=<?php the_permalink(); ?>" target="_blank">Share This on Google+</a></li> -->
								</ul>
							</footer>
						</article><!-- #post-## -->

						<?php get_sidebar(); ?>
					</div>
				</section>
<?php endwhile; ?>

<script>
// Opens share links in new window
$('#facebook-share').click(function(e){
	e.preventDefault();
	var sharer = "https://www.facebook.com/sharer/sharer.php?u=";
	window.open(sharer + location.href, 'sharer', 'width=626,height=436');
})
$('#twitter-share').click(function(e){
	e.preventDefault();
	var sharer = "http://twitter.com/home?status=Currently reading ";
	window.open(sharer + location.href, 'sharer', 'width=626,height=436');
})
</script>
<?php get_footer(); ?>
