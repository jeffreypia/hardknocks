<?php
/**
 * The template for displaying Author Archive pages.
 */

get_header(); ?>

<?php if ( have_posts() ) the_post(); ?>

				<section class="content content-main">
					<div class="inner">
						<h1><?php printf( __( 'Author Archives: %s', 'boilerplate' ), "<a class='url fn n' href='" . get_author_posts_url( get_the_author_meta( 'ID' ) ) . "' title='" . esc_attr( get_the_author() ) . "' rel='me'>" . get_the_author() . "</a>" ); ?></h1>

<?php
						rewind_posts();
						get_template_part( 'loop', 'author' );
?>
						<?php get_sidebar(); ?>
					</div><!-- .inner -->
				</section><!-- .content-main -->

				<?php hk_paginate() ?>

<?php get_footer(); ?>