<?php
/**
 * The loop that displays posts.
 */
?>

	<div class="articles">
<?php /* If there are no posts to display, such as an empty archive page */ ?>
<?php if ( ! have_posts() ) : ?>
		<article id="post-0" class="post error404 not-found">
			<h2 class="entry-title"><?php _e( 'Not Found', 'boilerplate' ); ?></h2>
			<div class="entry-content">
				<p><?php _e( 'Apologies, but no results were found for the requested archive. Perhaps searching will help find a related post.', 'boilerplate' ); ?></p>
				<?php get_search_form(); ?>
			</div><!-- .entry-content -->
		</article><!-- #post-0 -->
<?php endif; ?>

<?php
	/* Start the Loop.
	 *
	 * In Twenty Ten we use the same loop in multiple contexts.
	 * It is broken into three main parts: when we're displaying
	 * posts that are in the gallery category, when we're displaying
	 * posts in the asides category, and finally all other posts.
	 *
	 * Additionally, we sometimes check for whether we are on an
	 * archive page, a search page, etc., allowing for small differences
	 * in the loop on each template without actually duplicating
	 * the rest of the loop that is shared.
	 *
	 * Without further ado, the loop:
	 */ ?>
<?php while ( have_posts() ) : the_post(); ?>

<?php /* How to display posts in the Gallery category. */ ?>

		<?php if ( in_category( _x('gallery', 'gallery category slug', 'boilerplate') ) ) : ?>
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<h2 class="entry-title"><a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__( 'Permalink to %s', 'boilerplate' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark"><?php the_title(); ?></a></h2>

				<div class="entry-meta">
					<?php boilerplate_posted_on(); ?>
				</div><!-- .entry-meta -->

				<div class="entry-content">
<?php if ( post_password_required() ) : ?>
					<?php the_content(); ?>
<?php else : ?>
					<?php
						$images = get_children( array( 'post_parent' => $post->ID, 'post_type' => 'attachment', 'post_mime_type' => 'image', 'orderby' => 'menu_order', 'order' => 'ASC', 'numberposts' => 999 ) );
						if ( $images ) :
							$total_images = count( $images );
							$image = array_shift( $images );
							$image_img_tag = wp_get_attachment_image( $image->ID, 'thumbnail' );
					?>
							<div class="gallery-thumb">
								<a class="size-thumbnail" href="<?php the_permalink(); ?>"><?php echo $image_img_tag; ?></a>
							</div><!-- .gallery-thumb -->
							<p><em><?php printf( __( 'This gallery contains <a %1$s>%2$s photos</a>.', 'boilerplate' ),
									'href="' . get_permalink() . '" title="' . sprintf( esc_attr__( 'Permalink to %s', 'boilerplate' ), the_title_attribute( 'echo=0' ) ) . '" rel="bookmark"',
									$total_images
								); ?></em></p>
					<?php endif; ?>
							<?php the_excerpt(); ?>
<?php endif; ?>
				</div><!-- .entry-content -->

				<footer class="entry-utility">
					<a href="<?php echo get_term_link( _x('gallery', 'gallery category slug', 'boilerplate'), 'category' ); ?>" title="<?php esc_attr_e( 'View posts in the Gallery category', 'boilerplate' ); ?>"><?php _e( 'More Galleries', 'boilerplate' ); ?></a>
					|
					<?php comments_popup_link( __( 'Leave a comment', 'boilerplate' ), __( '1 Comment', 'boilerplate' ), __( '% Comments', 'boilerplate' ) ); ?>
					<?php edit_post_link( __( 'Edit', 'boilerplate' ), '|', '' ); ?>
				</footer><!-- .entry-utility -->
			</article><!-- #post-## -->

<?php /* How to display posts in the asides category */ ?>

		<?php elseif ( in_category( _x('asides', 'asides category slug', 'boilerplate') ) ) : ?>
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

			<?php if ( is_archive() || is_search() ) : // Display excerpts for archives and search. ?>
				<div class="entry-summary">
					<?php the_excerpt(); ?>
				</div><!-- .entry-summary -->
			<?php else : ?>
				<div class="entry-content">
					<?php the_content( __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'boilerplate' ) ); ?>
				</div><!-- .entry-content -->
			<?php endif; ?>

				<footer class="entry-utility">
					<?php boilerplate_posted_on(); ?>
					|
					<?php comments_popup_link( __( 'Leave a comment', 'boilerplate' ), __( '1 Comment', 'boilerplate' ), __( '% Comments', 'boilerplate' ) ); ?>
					<?php edit_post_link( __( 'Edit', 'boilerplate' ), '| ', '' ); ?>
				</footer><!-- .entry-utility -->
			</article><!-- #post-## -->

<?php /* How to display all other posts. */ ?>

		<?php else : ?>
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<header>
					<div class="entry-thumb"><a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__( 'Permalink to %s', 'boilerplate' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark"><?php the_post_thumbnail('featured-image') ?></a></div>
					<div class="entry-meta">
						<?php boilerplate_posted_on(); ?>
					</div><!-- .entry-meta -->
				</header>

				<h2 class="entry-title"><a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__( 'Permalink to %s', 'boilerplate' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark"><?php the_title(); ?></a></h2>

				<div class="entry-content">
					<?php the_excerpt(); ?>
					<?php edit_post_link( __( 'Edit', 'boilerplate' ), '', '' ); ?>
				</div><!-- .entry-content -->
			</article><!-- #post-## -->
		<?php endif; // This was the if statement that broke the loop into three parts based on categories. ?>

<?php endwhile; // End the loop. Whew. ?>

	</div><!-- .articles -->

