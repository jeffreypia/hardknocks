<?php
/**
 * Template name: Sales
 * The template for Group Events page.
 */

	get_header();
?>

<?php
	if ( have_posts() ) while ( have_posts() ) :
		the_post();

		$mobileThumbURL = get('page_options_mobile_thumbnail');
		$mobileThumbID = hk_get_attachment_id_from_src($mobileThumbURL);
		$mobileThumb = wp_get_attachment_image( $mobileThumbID, 'thumbnail-320x320' );

		$buttonLink = get('page_options_banner_button_link');
		$buttonLink = get('page_options_banner_button_link');
		$buttonText = get('page_options_banner_button_text');
		$modalLink = get('page_options_banner_modal_link');
		$banners = get_group('banner');
?>
				<section class="banner slider">
		<?php if( $mobileThumb ) : ?>
					<div class="mobile-thumb">
						<?php echo $mobileThumb; ?>
					</div>
		<?php endif; ?>

					<?php if( $banners ) : ?>					
					<ul class="slides">
<?php
		$i = 0;
		foreach( $banners as $banner ) :
			$postThumbURL = $banner['banner_image'][1]['original'];
			$postThumbID = hk_get_attachment_id_from_src( $postThumbURL );
			$bannerImage = wp_get_attachment_image( $postThumbID, 'banner-1600x550', false, array( 'class' => 'banner-image' ) );
			$buttonLink = $banner['banner_button_link'][1];
			$buttonText = $banner['banner_button_text'][1];
			$i++;
?>
						<li class="slide">
							<?php echo $bannerImage; ?>
							<div class="page-meta">
								<?php if( $i === 1 ) : ?>
								<h1 class="heading-main"><?php echo strip_tags( $banner['banner_heading_main'][1], '<br>' ); ?></h1>
								<?php else : ?>
								<p class="heading-main"><?php echo strip_tags( $banner['banner_heading_main'][1], '<br>' ); ?></p>
								<?php endif; ?>
								<p class="heading-sub"><?php echo strip_tags( $banner['banner_heading_sub'][1], '<br>' ); ?></p>
								<?php if( $buttonLink ) : ?>
									<a class="button" href="<?php echo $buttonLink; ?>"><?php echo $buttonText ? $buttonText : 'Find Out More' ?></a>
								<?php endif; ?>
								<?php edit_post_link( __( 'Edit', 'boilerplate' ), '', '' ); ?>
							</div>
						</li>
		<?php endforeach; ?>
					</ul>
	<?php else : ?>
					<?php the_post_thumbnail( 'banner-1600x480', array('class'=>'banner-image') ); ?>
					<div class="page-meta">
						<p class="heading-main"><?php echo strip_tags( get('page_options_banner_heading_main'), '<br>' ); ?></p>
						<p class="heading-sub"><?php echo strip_tags( get('page_options_banner_heading_sub'), '<br>' ); ?></p>
						<?php if( $buttonLink ) : ?>
							<a class="button" href="<?php echo get_permalink( $buttonLink); ?>"><?php echo $buttonText ? $buttonText : 'Find Out More' ?></a>
						<?php endif; ?>
						<?php edit_post_link( __( 'Edit', 'boilerplate' ), '', '' ); ?>
					</div>
	<?php endif; ?>
					</div>
				</section>

				<section class="children-list content-main">
					<div class="inner">
						<h2 class="section-title">All Events</h2>
						<ul class="children">
<?php
							$args = array(
								'posts_per_page'	=> -1,
								'post_type'			=> 'group_sale',
								'orderby' 			=> 'menu_order',
								'order' 			=> 'ASC',
							);
							$subPages = get_posts( $args );
							global $post;
							foreach( $subPages as $subPage ) :
								$postThumbURL = get_image('page_options_thumbnail', 1, 1, 0, $subPage->ID);
								$postThumbID = hk_get_attachment_id_from_src( $postThumbURL );
								$postThumb = wp_get_attachment_image( $postThumbID, 'thumbnail-370x370' );
?>
							<li class="child">
								<?php echo $postThumb; ?>
								<a class="child-link" href="<?php echo get_permalink( $subPage->ID); ?>">
									<span class="title"><?php echo get_the_title( $subPage->ID ); ?></span>
									<span class="brief"><?php echo get('page_options_brief',1,1,$subPage->ID); ?></span>
								</a>
							</li>
							<?php endforeach; ?>
						</div>
					</ul>
				</section>
<?php endwhile; ?>

<?php get_footer(); ?>