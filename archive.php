<?php
/**
 * The template for displaying Archive pages.
 */

get_header(); ?>

				<section class="content content-main">
					<div class="inner">
<?php if ( have_posts() ) the_post(); ?>
						<h1 class="page-title"><?php
							if ( is_day() ) :
								printf( __( 'Daily Archives: %s', 'boilerplate' ), get_the_date() );
							elseif ( is_month() ) :
								printf( __( 'Monthly Archives: %s', 'boilerplate' ), get_the_date('F Y') );
							elseif ( is_year() ) :
								printf( __( 'Yearly Archives: %s', 'boilerplate' ), get_the_date('Y') );
							else :
								_e( 'Blog Archives', 'boilerplate' );
							endif;
						?></h1>
<?php
						rewind_posts();
						 get_template_part( 'loop', 'archive' );
?>
						<?php get_sidebar(); ?>
					</div><!-- .inner -->
				</section><!-- .content-main -->

<?php get_footer(); ?>
