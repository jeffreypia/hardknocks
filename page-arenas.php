<?php
/**
 * The template for Arenas pages.
 */

get_header();
$parentID = $post->post_parent;
?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
				<section class="banner">
					<?php the_post_thumbnail( 'banner-1600x800', array('class'=>'banner-image') ); ?>
					<div class="page-meta">
						<h1 class="page-title"><?php the_title(); ?></h1>
						<p class="heading-main"><?php echo strip_tags( get('page_options_banner_heading_main'), '<br>' ); ?></p>
						<p class="heading-sub"><?php echo strip_tags( get('page_options_banner_heading_sub'), '<br>' ); ?></p>
						<?php echo get('page_options_banner_copy'); ?>
						<?php edit_post_link( __( 'Edit', 'boilerplate' ), '', '' ); ?>
					</div>
				</section>
<?php endwhile; ?>

				<nav class="subnav">
					<div class="inner">
						<ul class="menu">
							<li class="menu-item"><a href="#">Overview</a></li>
							<li class="menu-item active"><a href="#">Arenas</a></li>
							<li class="menu-item"><a href="#">Share</a></li>
							<li class="menu-item book-now"><a href="#">Book Now</a></li>
						</ul>
					</div>
				</nav>

				<section class="arena-list content-main">
					<div class="inner">
						<h2 class="section-title">Arenas</h2>
						<ul class="arenas">
<?php
							$args = array(
								'posts_per_page' 	=> -1,
								'post_type' 		=> 'arena',
								'orderby' 			=> 'menu_order',
								'order' 			=> 'ASC',
							);
							$arenas = get_posts( $args ); 

							// Loop through arenas
							foreach( $arenas as $arena ):
?>
							<li class="arena">
								<div class="thumbnail"><?php echo get_the_post_thumbnail($arena->ID, 'thumbnail-570x370'); ?></div>
								<div class="title"><?php echo get_the_title( $arena->ID ); ?></div>
								<div class="description"><?php echo $arena->post_content; ?></div>
								<?php edit_post_link( __( 'Edit', 'boilerplate' ), '', '', $arena->ID ); ?>
							</li>
							<?php endforeach; ?>
						</ul>
					</div>
				</section>

				<section class="sibling-list content-sub">
					<div class="inner">
						<h2 class="section-title"><?php echo get_the_title($parentID); ?></h2>
						<ul class="siblings">
<?php
							$args = array(
								'posts_per_page'	=> -1,
								'post_type'			=> 'page',
								'post_parent'		=> $parentID,
								'orderby' 			=> 'menu_order',
								'order' 			=> 'ASC',
							);
							$subPages = get_posts( $args );
							global $post;
							foreach( $subPages as $subPage ) :
								$postThumbURL = get_image('page_options_thumbnail', 1, 1, 0, $subPage->ID);
								$postThumbID = hk_get_attachment_id_from_src( $postThumbURL );
								$postThumb = wp_get_attachment_image( $postThumbID, 'thumbnail-370x370' );
?>
							<li class="sibling">
								<?php echo $postThumb; ?>
								<a class="sibling-link" href="<?php echo get_permalink( $subPage->ID); ?>">
									<span class="title"><?php echo get_the_title( $subPage->ID ); ?></span>
								</a>
							</li>
							<?php endforeach; ?>
						</ul>
					</div>
				</section>

<?php get_footer(); ?>