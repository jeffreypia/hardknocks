<?php
/**
 * Template name: Gallery - Large
 * Template for 2-column gallery pages
 */

	get_header();
?>

<?php
	if ( have_posts() ) while ( have_posts() ) :
		the_post();

		$parentID = $post->post_parent;

		$mobileThumbURL = get('page_options_mobile_thumbnail');
		$mobileThumbID = hk_get_attachment_id_from_src($mobileThumbURL);
		$mobileThumb = wp_get_attachment_image( $mobileThumbID, 'thumbnail-320x320' );
		$mobileThumbURL = wp_get_attachment_image_src( $mobileThumbID, $size='thumbnail-320x320' );
		$desktopThumbURL = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), $size='banner-1600x550' );

		$banners = get_group('banner');

		if( $banners ) :
?>
				<section id="section-overview" class="banner slider">
					<?php if( $mobileThumb ) : ?>
					<div class="mobile-thumb">
						<?php echo $mobileThumb; ?>
					</div>
					<?php endif; ?>
					<ul class="slides">
<?php
			$i = 0;
			foreach( $banners as $banner ) :
				$postThumbURL = $banner['banner_image'][1]['original'];
				$postThumbID = hk_get_attachment_id_from_src( $postThumbURL );
				$bannerImage = wp_get_attachment_image( $postThumbID, 'banner-1600x550', false, array( 'class' => 'banner-image' ) );
				$buttonLink = $banner['banner_button_link'][1];
				$buttonText = $banner['banner_button_text'][1];
				$i++;
?>
						<li class="slide">
							<?php echo $bannerImage; ?>
							<div class="page-meta">
								<?php if( $i === 1 ) : ?>
								<h1 class="heading-main"><?php echo strip_tags( $banner['banner_heading_main'][1], '<br>' ); ?></h1>
								<?php else : ?>
								<p class="heading-main"><?php echo strip_tags( $banner['banner_heading_main'][1], '<br>' ); ?></p>
								<?php endif; ?>

								<p class="heading-sub"><?php echo strip_tags( $banner['banner_heading_sub'][1], '<br>' ); ?></p>
								<?php if( $buttonLink ) : ?>
									<a class="button" href="<?php echo $buttonLink; ?>"><?php echo $buttonText ? $buttonText : 'Find Out More' ?></a>
								<?php endif; ?>
								<?php edit_post_link( __( 'Edit', 'boilerplate' ), '', '' ); ?>
							</div>
						</li>
			<?php endforeach; ?>
					</ul>
				</section>

<?php
		else :
				$headingMain = get('page_options_banner_heading_main');
				$headingSub = get('page_options_banner_heading_sub');
?>

				<section id="section-overview" class="banner" data-small="<?php echo $mobileThumbURL[0]; ?>" data-large="<?php echo $desktopThumbURL[0]; ?>">
					<img class="banner-image" src="">
					<div class="page-meta">
					
					<?php if( $parentID ) : ?>

						<h1 class="page-title"><?php the_title(); ?></h1>
						<?php if( $headingMain ) { ?><p class="heading-main"><?php echo strip_tags( get('page_options_banner_heading_main'), '<br>' ); ?></p><?php } ?>
					
					<?php else : ?>
					
						<h1 class="heading-main"><?php echo $headingMain ? strip_tags( get('page_options_banner_heading_main'), '<br>' ) : get_the_title(); ?></h1>
					
					<?php endif; ?>

						<p class="heading-sub"><?php echo strip_tags( get('page_options_banner_heading_sub'), '<br>' ); ?></p>
						<?php if( $buttonLink ) : ?>
							<a class="button" href="<?php echo get_permalink( $buttonLink); ?>"><?php echo $buttonText ? $buttonText : 'Find Out More' ?></a>
						<?php endif; ?>
						<?php edit_post_link( __( 'Edit', 'boilerplate' ), '', '' ); ?>
					</div>
				</section>
		<?php endif; ?>

				<nav class="subnav">
					<div class="inner">
						<ul class="menu">
							<li class="menu-item"><a href="#" data-bookmark="banner">Overview</a></li>
							<li class="menu-item"><a href="#" data-bookmark="content-main"><?php the_title(); ?></a></li>
							<li class="menu-item"><a href="#" data-modal="share">Share</a></li>
							<li class="menu-item book-now"><a href="#" data-modal="book-now">Book Now</a></li>
						</ul>
					</div>
				</nav>

				<section class="gallery-item-list content-main">
					<div class="inner">
						<h2 class="section-title"><?php the_title(); ?></h2>
						<ul class="gallery-items">
<?php
							$galleryPage = get_page_by_path( $post->post_name, OBJECT, 'galleries');
							$galleryItems = get_group( 'gallery_item', $galleryPage->ID );

							// Loop through arenas
							foreach( $galleryItems as $galleryItem ):
								$youTubeID = $galleryItem['gallery_item_youtube_id'][1];
								$postThumbURL = $galleryItem['gallery_item_graphic'][1]['original'];
								$postThumbID = hk_get_attachment_id_from_src( $postThumbURL );
								$postThumb = wp_get_attachment_image( $postThumbID, 'thumbnail-570x390' );
								$postFull = wp_get_attachment_image_src( $postThumbID, 'full' );
?>
							<li class="gallery-item">
								<?php if( $youTubeID ) : ?>
								<div class="video"><iframe width="570" height="390" src="http://www.youtube.com/embed/<?php echo( $youTubeID ); ?>?wmode=transparent" frameborder="0" allowfullscreen></iframe></div>
								<?php elseif( $postThumb ) : ?>
								<div class="thumbnail" data-modal="gallery" data-src="<?php echo $postFull[0]; ?>" data-width="<?php echo $postFull[1]; ?>" data-height="<?php echo $postFull[2]; ?>"><?php echo $postThumb; ?></div>
								<?php endif; ?>
								<h2 class="title"><?php echo $galleryItem['gallery_item_title'][1]; ?></h2>
								<div class="description"><?php echo $galleryItem['gallery_item_description'][1]; ?></div>
								<?php edit_post_link( __( 'Edit', 'boilerplate' ), '', '', $galleryItem->ID ); ?>
							</li>
							<?php endforeach; ?>
						</ul>
					</div>
				</section>
<?php endwhile; ?>

<?php if( $parentID ) : ?>
				<section class="sibling-list content-sub">
					<div class="inner">
						<h2 class="section-title"><?php echo get_the_title($parentID); ?></h2>
						<ul class="siblings">
<?php
							$args = array(
								'posts_per_page'	=> -1,
								'post_type'			=> 'page',
								'post_parent'		=> $parentID,
								'orderby' 			=> 'menu_order',
								'order' 			=> 'ASC',
							);
							$subPages = get_posts( $args );
							global $post;
							foreach( $subPages as $subPage ) :
								$postThumbURL = get_image('page_options_thumbnail', 1, 1, 0, $subPage->ID);
								$postThumbID = hk_get_attachment_id_from_src( $postThumbURL );
								$postThumb = wp_get_attachment_image( $postThumbID, 'thumbnail-370x370' );
?>
							<li class="sibling">
								<?php echo $postThumb; ?>
								<a class="sibling-link" href="<?php echo get_permalink( $subPage->ID); ?>">
									<span class="title"><?php echo get_the_title( $subPage->ID ); ?></span>
								</a>
							</li>
							<?php endforeach; ?>
						</div>
					</ul>
				</section>
<?php endif; ?>

<div id="modal-gallery" class="modal"><img src="" alt=""><span class="cancel">&times;</span></div>
<?php get_footer(); ?>