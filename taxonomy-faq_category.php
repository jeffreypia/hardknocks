<?php
/**
 * The template for displaying FAQ Categories pages.
 */

get_header();
global $query_string;
query_posts( $query_string .'&posts_per_page=-1' );
?>

				<section class="content content-main">
					<div class="inner">
						<?php if ( have_posts() ) the_post(); ?>
						<h1 class="page-title"><?php printf( __( 'FAQ Category Archives: %s', 'boilerplate' ), '' . single_cat_title( '', false ) . '' ); ?></h1>
						<?php rewind_posts(); ?>

							<div class="articles">
<?php
								while ( have_posts() ) :
									the_post();
									$youTubeID = get('faq_options_youtube_id');
?>
								<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
									<?php if( $youTubeID ) : ?>
									<div class="video"><iframe width="870" height="500" src="http://www.youtube.com/embed/<?php echo( $youTubeID ); ?>?wmode=transparent" frameborder="0" allowfullscreen></iframe></div>
									<?php elseif( has_post_thumbnail() ) : ?>
									<div class="entry-thumb"><?php the_post_thumbnail('featured-image'); ?></div>
									<?php endif; ?>

									<h2 class="entry-title"><?php the_title(); ?></h2>

									<div class="entry-content">
										<?php the_content(); ?>
										<?php edit_post_link( __( 'Edit', 'boilerplate' ), '', '' ); ?>
									</div><!-- .entry-content -->
								</article><!-- #post-## -->
								<?php endwhile; ?>
							</div><!-- .articles -->
						<?php get_sidebar('faq'); ?>
					</div><!-- .inner -->
				</section><!-- .content-main -->

<?php get_footer(); ?>
