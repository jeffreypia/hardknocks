<?php
/**
 * The template for Group Sale Detail page.
 */

get_header();
$parent = get_page_by_path( 'group-sales' );
$parentID = $parent->ID;
$bannerTestimonial = get_image('testimonials_image', 1, 1, 0);
$bannerEvent = get_image('event_specs_image', 1, 1, 0);
?>

<?php
	if ( have_posts() ) while ( have_posts() ) :
		the_post();

		$mobileThumbURL = get('page_options_mobile_thumbnail');
		$mobileThumbID = hk_get_attachment_id_from_src($mobileThumbURL);
		$mobileThumbURL = wp_get_attachment_image_src( $mobileThumbID, $size='thumbnail-320x320' );
		$desktopThumbURL = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), $size='banner-1600x550' );

		$headingMain = strip_tags( get('page_options_banner_heading_main'), '<br>' );
		$buttonLink = get('page_options_banner_button_link');
		$buttonText = get('page_options_banner_button_text');
		$modalLink = get('page_options_banner_modal_link');

		$galleryImages = get_field('gallery_image');
?>
				<?php if( $mobileThumbURL || $desktopThumbURL ) : ?>
				<section class="banner" data-small="<?php echo $mobileThumbURL[0]; ?>" data-large="<?php echo $desktopThumbURL[0]; ?>">
					<img class="banner-image" src="">
					<div class="page-meta">
						<p class="page-title"><?php echo get_the_title($parentID); ?></p>
						<h1 class="heading-main"><?php echo $headingMain ? $headingMain : the_title(); ?></h1>
						<p class="heading-sub"><?php echo strip_tags( get('page_options_banner_heading_sub'), '<br>' ); ?></p>
						<?php echo get('page_options_banner_copy'); ?>
						<?php if( $modalLink && $modalLink !== 'none' ) : ?>
						<a class="button" href="#" data-modal="<?php echo $modalLink; ?>"><?php echo $buttonText ? $buttonText : 'Find Out More' ?></a>
						<?php elseif( $buttonLink ) : ?>
						<a class="button" href="<?php echo $buttonLink; ?>"><?php echo $buttonText ? $buttonText : 'Find Out More' ?></a>
						<?php endif; ?>
						<?php edit_post_link( __( 'Edit', 'boilerplate' ), '', '' ); ?>
					</div>
				</section>
				<?php endif; ?>

				<nav class="subnav">
					<div class="inner">
						<ul class="menu">
							<li class="menu-item"><a href="#" data-bookmark="banner">Overview</a></li>
							<?php if( $bannerEvent ) { ?><li class="menu-item"><a href="#" data-bookmark="event-specs">Event Specs</a></li><?php } ?>
							<?php if( $galleryImages ) { ?><li class="menu-item"><a href="#" data-bookmark="gallery">Gallery</a></li><?php } ?>
							<?php if( $bannerTestimonial ) { ?><li class="menu-item"><a href="#" data-bookmark="testimonials">Testimonials</a></li><?php } ?>
							<li class="menu-item"><a href="#" data-bookmark="sibling-list">All Events</a></li>
							<li class="menu-item"><a href="#" data-modal="share">Share</a></li>
							<li class="menu-item"><a href="#" data-modal="quote-request">Request a Quote</a></li>
							<li class="menu-item book-now"><a href="#" data-modal="book-now">Book Now</a></li>
						</ul>
					</div>
				</nav>

				<section class="mobile-copy banner">
					<div class="inner">
						<p class="heading-sub"><?php echo strip_tags( get('page_options_banner_heading_sub'), '<br>' ); ?></p>
						<?php echo get('page_options_banner_copy'); ?>
					</div>
				</section>

<?php
				if( $bannerEvent ) :
					$postThumbID = hk_get_attachment_id_from_src( $bannerEvent );
					// $bannerEvent = wp_get_attachment_image( $postThumbID, 'banner-1600x800', false, array( 'class' => 'banner-image' ) );
					$headingMain = get('event_specs_heading_main');
					$alsoIncludedItems = get_field( 'event_specs_also_included' );
					$buttonLink = get('event_specs_button_link');
					$buttonText = get('event_specs_button_text');
					$modalLink = get('event_specs_modal_link');
					$buttonLink2 = get('event_specs_button_2_link');
					$buttonText2 = get('event_specs_button_2_text');
					$modalLink2 = get('event_specs_modal_link_2');
?>
				<section class="event-specs banner" style="background: url(<?php echo $bannerEvent ?>)">
					<div class="page-meta">
						<header>
							<div class="header-meta">
								<p class="page-title"><?php the_title(); ?></p>
								<?php if( $headingMain ) { ?><h2 class="heading-main"><?php echo $headingMain ?></h2><?php } ?>
							</div>
						</header>

						<ul class="callouts">
							<li class="callout callout-1"><?php echo get('event_specs_icon_1') ?></li>
							<li class="callout callout-3"><?php echo get('event_specs_icon_3') ?></li>
						</ul>
						<ul class="callouts">
							<li class="callout callout-2"><?php echo get('event_specs_icon_2') ?></li>
							<li class="callout callout-4"><?php echo get('event_specs_icon_4') ?></li>
						</ul>

						<footer>
							<h3>Also Included</h3>
							<ul class="also-included">
								<?php foreach( $alsoIncludedItems as $alsoIncludedItem ) : ?>
								<li><?php echo $alsoIncludedItem ?></li>
								<?php endforeach; ?>
							</ul>

							<?php if( $modalLink && $modalLink !== 'none' ) : ?>
							<a class="button" href="#" data-modal="<?php echo $modalLink; ?>"><?php echo $buttonText ? $buttonText : 'Find Out More' ?></a>
							<?php elseif( $buttonLink ) : ?>
							<a class="button" href="<?php echo $buttonLink; ?>"><?php echo $buttonText ? $buttonText : 'Find Out More' ?></a>
							<?php endif; ?>

							<?php if( $modalLink2 && $modalLink2 !== 'none' ) : ?>
							<a class="button" href="#" data-modal="<?php echo $modalLink2; ?>"><?php echo $buttonText2 ? $buttonText2 : 'Find Out More' ?></a>
							<?php elseif( $buttonLink2 ) : ?>
							<a class="button" href="<?php echo $buttonLink2; ?>"><?php echo $buttonText2 ? $buttonText2 : 'Find Out More' ?></a>
							<?php endif; ?>
						</footer>
					</div><!-- .page-meta -->
				</section>
				<?php endif; ?>

				<?php if( $galleryImages ) : ?>
				<section class="gallery banner">
					<ul class="slides">
						<?php foreach( $galleryImages as $galleryImage ) : ?>
						<li class="slide">
							<img src="<?php echo $galleryImage['original'] ?>">
						</li>
						<?php endforeach; ?>
					</ul>
				</section>
				<?php endif; ?>

				<?php if( $bannerTestimonial ) : ?>
				<section class="testimonials banner">
					<ul class="slides">
<?php
						$testimonials = get_group('testimonials');
						foreach( $testimonials as $testimonial ) :
							$postThumbURL = $testimonial['testimonials_image'][1]['original'];
							$postThumbID = hk_get_attachment_id_from_src( $postThumbURL );
							$bannerTestimonial = wp_get_attachment_image( $postThumbID, 'banner-1600x800', false, array( 'class' => 'banner-image' ) );
							$buttonLink = $testimonial['testimonials_button_link'][1];
							$buttonText = $testimonial['testimonials_button_text'][1];
							$modalLink = $testimonial['testimonials_modal_link'][1];
?>
						<li class="slide">
							<?php echo $bannerTestimonial; ?>
							<div class="page-meta">
								<p class="page-title"><?php the_title(); ?></p>
								<p class="heading-main"><?php echo strip_tags( $testimonial['testimonials_heading_main'][1], '<br>' ); ?></p>
								<p class="quote"><?php echo strip_tags( $testimonial['testimonials_quote'][1], '<br>' ); ?></p>
								<p class="source"><?php echo strip_tags( $testimonial['testimonials_source'][1], '<br>' ); ?></p>
								<?php if( $modalLink && $modalLink !== 'none' ) : ?>
								<a class="button" href="#" data-modal="<?php echo $modalLink; ?>"><?php echo $buttonText ? $buttonText : 'Find Out More' ?></a>
								<?php elseif( $buttonLink ) : ?>
								<a class="button" href="<?php echo $buttonLink; ?>"><?php echo $buttonText ? $buttonText : 'Find Out More' ?></a>
								<?php endif; ?>
							</div>
						</li>
						<?php endforeach; ?>
					</ul>
				</section>
				<?php endif; ?>

				<section class="sibling-list content-sub">
					<div class="inner">
						<h2 class="section-title">All Events</h2>
						<ul class="siblings">
<?php
							$args = array(
								'posts_per_page'	=> -1,
								'post_type'			=> 'group_sale',
								'orderby' 			=> 'menu_order',
								'order' 			=> 'ASC',
							);
							$subPages = get_posts( $args );
							global $post;
							foreach( $subPages as $subPage ) :
								$postThumbURL = get_image('page_options_thumbnail', 1, 1, 0, $subPage->ID);
								$postThumbID = hk_get_attachment_id_from_src( $postThumbURL );
								$postThumb = wp_get_attachment_image( $postThumbID, 'thumbnail-370x370' );
?>
							<li class="child">
								<?php echo $postThumb; ?>
								<a class="child-link" href="<?php echo get_permalink( $subPage->ID); ?>">
									<span class="title"><?php echo get_the_title( $subPage->ID ); ?></span>
									<span class="brief"><?php echo get('page_options_brief',1,1,$subPage->ID); ?></span>
								</a>
							</li>
							<?php endforeach; ?>
						</div>
					</ul>
				</section>
<?php endwhile; ?>

<?php get_footer(); ?>