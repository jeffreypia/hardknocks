<?php
/**
 * Template name: FAQ
 * Template for FAQ page
 */

get_header();

	if ( have_posts() ) while ( have_posts() ) :
		the_post();

		$mobileThumbURL = get('page_options_mobile_thumbnail');
		$mobileThumbID = hk_get_attachment_id_from_src($mobileThumbURL);
		$mobileThumbURL = wp_get_attachment_image_src( $mobileThumbID, $size='thumbnail-320x320' );
		$desktopThumbURL = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), $size='banner-1600x550' );
?>
				<?php if( $mobileThumbURL || $desktopThumbURL ) : ?>
				<section class="banner" data-small="<?php echo $mobileThumbURL[0]; ?>" data-large="<?php echo $desktopThumbURL[0]; ?>">
					<img class="banner-image" src="">
				</section>
				<?php endif; ?>
<?php endwhile; ?>

<?php
	$args = array(
		'posts_per_page' 	=> -1,
		'post_type' 		=> 'faqs',
		'orderby' 			=> 'menu_order',
		'order' 			=> 'DESC',
	);
	$faqs = get_posts( $args ); 
?>
				<section class="content content-main">
					<div class="inner">
						<h1 class="entry-title"><?php the_title(); ?></h1>
						<div class="articles">
<?php
							foreach( $faqs as $post ) :
								setup_postdata($post);
								$youTubeID = get('faq_options_youtube_id');
?>
								<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
									<?php if( $youTubeID ) : ?>
									<div class="video"><iframe width="870" height="500" src="http://www.youtube.com/embed/<?php echo( $youTubeID ); ?>?wmode=transparent" frameborder="0" allowfullscreen></iframe></div>
									<?php elseif( has_post_thumbnail() ): ?>
									<div class="entry-thumb"><?php the_post_thumbnail('featured-image'); ?></div>
									<?php endif; ?>

									<h2 class="entry-title"><?php the_title(); ?></h2>

									<div class="entry-content">
										<?php the_content(); ?>
										<?php edit_post_link( __( 'Edit', 'boilerplate' ), '', '' ); ?>
									</div><!-- .entry-content -->
								</article><!-- #post-## -->
							<?php endforeach; ?>
						</div><!-- .articles -->

						<?php get_sidebar('faq'); ?>
					</div><!-- .inner -->
				</section><!-- .content-main -->

				<?php hk_paginate() ?>

<?php get_footer(); ?>
