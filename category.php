<?php
/**
 * The template for displaying Category Archive pages.
 */

get_header(); ?>

				<section class="content content-main">
					<div class="inner">
						<h1 class="page-title"><?php printf( __( 'Category Archives: %s', 'boilerplate' ), '' . single_cat_title( '', false ) . '' ); ?></h1>
<?php
						$category_description = category_description();
						if ( ! empty( $category_description ) )
							echo '' . $category_description . '';

						get_template_part( 'loop', 'category' );
?>
						<?php get_sidebar(); ?>
					</div><!-- .inner -->
				</section><!-- .content-main -->

				<?php hk_paginate() ?>

<?php get_footer(); ?>
