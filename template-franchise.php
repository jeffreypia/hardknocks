<?php
/**
 * Template name: Franchise
 * Template for Franchise page
 */

get_header();
?>

<?php
	if ( have_posts() ) while ( have_posts() ) :
		the_post();
		
		$mobileThumbURL = get('page_options_mobile_thumbnail');
		$mobileThumbID = hk_get_attachment_id_from_src($mobileThumbURL);
		$mobileThumbURL = wp_get_attachment_image_src( $mobileThumbID, $size='thumbnail-320x320' );
		$desktopThumbURL = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), $size='banner-1600x550' );
?>
				<?php if( $mobileThumbURL || $desktopThumbURL ) : ?>
				<section class="banner" data-small="<?php echo $mobileThumbURL[0]; ?>" data-large="<?php echo $desktopThumbURL[0]; ?>">
					<img class="banner-image" src="">
					<div class="page-meta">
						<h1 class="page-title"><?php the_title(); ?></h1>
						<p class="heading-main"><?php echo strip_tags( get('page_options_banner_heading_main'), '<br>' ); ?></p>
						<p class="heading-sub"><?php echo strip_tags( get('page_options_banner_heading_sub'), '<br>' ); ?></p>
						<?php echo get('page_options_banner_copy'); ?>
						<?php edit_post_link( __( 'Edit', 'boilerplate' ), '', '' ); ?>
					</div>
				</section>
				<?php endif; ?>

				<section class="content content-main">
					<div class="inner">
						<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
							<h2 class="entry-title"><?php the_title(); ?></h2>
							<div class="entry-content">
								<?php the_content(); ?>
								<?php edit_post_link( __( 'Edit', 'boilerplate' ), '', '' ); ?>
							</div><!-- .entry-content -->
						</article><!-- #post-## -->


						<aside>
							<div class="entry-content">
								<h3 class="entry-title">Interested in opening a franchise with Hard Knocks?</h3>
								<span class="button" data-modal="franchise">Request Franchise Info</span>
							</div>
						</aside>
<?php endwhile; ?>
					</div><!-- .inner -->
				</section>

<div class="modal" id="modal-franchise">
	<?php echo do_shortcode( '[contact-form-7 id="381" title="Franchise"]' ) ?>
</div>
<?php get_footer(); ?>