<?php
/**
 * The template for Location Detail page.
 */

get_header();
?>

<?php
	if ( have_posts() ) while ( have_posts() ) : the_post();
		$googleMap = get('page_options_google_map_link');
		$hoursScheduleItems = get_group('hours_schedule');
		$locationInfoItems = get_group('location_info');
		$slides = get_group('slides');
		$additionalInfoHeading = get('additional_info_heading');
		$additionalInfoCopy = get('additional_info_copy');
		$currentID = $post->ID;
		$groupSales = get_field('group_sales_item');
		$galleryImages = get_field('gallery_image');
		$postsList = get_posts('tag='.$post->post_name.'&posts_per_page=1');
		$mobileThumbURL = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), $size='thumbnail-800x450' );
		$desktopThumbURL = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), $size='banner-1600x550' );
		$wordCount = get('news_options_word_count');
		$lat = get('map_options_lat');
		$lng = get('map_options_lng');

?>

				<?php if( $lat && $lng ) : ?>

				<section class="map banner" id="map-canvas"></section>
				<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
				<script type="text/javascript">
					var hk_location = new google.maps.LatLng(<?php echo $lat; ?>,<?php echo $lng; ?>),
					marker,
					map;
					function initialize() {
						var styles = [
							{
							    "stylers": [
						      		{ "saturation": -100 }
						    	]
						    },
						    {
							}
						];
						var mapOptions = {
							mapTypeControlOptions: {
								mapTypeIds: [ 'Styled']
							},
							zoom: 14,
							mapTypeId: 'Styled',
							center: hk_location,
							draggable: false,
							scrollwheel: false, 
							disableDefaultUI: true
							// panControl: false,
							// zoomControl: false,
							// scaleControl: false,
							// streetViewControl: false,
							// disableDoubleClickZoom: true
						};
						map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

						var styledMapType = new google.maps.StyledMapType(styles, { name: 'Styled' });
						map.mapTypes.set('Styled', styledMapType);

						marker = new google.maps.Marker({
							map:map,
							draggable:false,
							animation: google.maps.Animation.DROP,
							icon: 'http://www.googlemapsmarkers.com/v1/A/5ea939/FFFFFF/5ea939/',	
							position: hk_location
						});
						google.maps.event.addListener(marker, 'click', toggleBounce);
						google.maps.event.addDomListener(window, 'resize', function() {
						    map.setCenter(hk_location);
						});
						
					}

					function toggleBounce() {
						if (marker.getAnimation() != null) {
							marker.setAnimation(null);
						} else {
							marker.setAnimation(google.maps.Animation.BOUNCE);
						}
					}
					google.maps.event.addDomListener(window, 'load', initialize);
				</script>

				<?php elseif( $googleMap ) : ?>

				<section class="map banner">
					<?php echo $googleMap; ?>
				</section>
				
				<?php else : ?>
				
				<section class="banner" data-small="<?php echo $mobileThumbURL[0]; ?>" data-large="<?php echo $desktopThumbURL[0]; ?>">
					<img class="banner-image" src="">
				</section>

				<?php endif; ?>

				<nav class="subnav">
					<div class="inner">
						<ul class="menu">
							<li class="menu-item"><a href="#" data-bookmark="location-info">Location Info</a></li>
							<li class="menu-item"><a href="#" data-bookmark="hours-schedule">Hours &amp; Schedule</a></li>
							<?php if( $groupSales ) { ?><li class="menu-item"><a href="#" data-bookmark="event-specs">Groups &amp; Parties</a></li><?php } ?>
							<?php if( $galleryImages ) { ?><li class="menu-item"><a href="#" data-bookmark="gallery">Gallery</a></li><?php } ?>
							<?php if( $postsList ) { ?><li class="menu-item"><a href="#" data-bookmark="news-events">News</a></li><?php } ?>
							<li class="menu-item"><a href="#" data-bookmark="sibling-list">Other Locations</a></li>
							<li class="menu-item book-now"><a href="#" data-modal="book-now">Book Now</a></li>
						</ul>
					</div>
				</nav>

				<section class="location-info content-main">
					<div class="inner">
						<h1 class="section-title"><?php the_title(); ?></h1>
						<?php foreach( $locationInfoItems as $locationInfo ) : ?>
						<article>
							<h3 class="entry-title"><?php echo $locationInfo['location_info_heading'][1]; ?></h3>
							<div class="entry-content"><?php echo $locationInfo['location_info_details'][1]; ?></div>
						</article>
						<?php endforeach; ?>

						<?php if( $additionalInfoHeading || $additionalInfoCopy ) : ?>
						<article class="location-details">
							<h2 class="entry-title"><?php echo $additionalInfoHeading; ?></h2>
							<div class="entry-content">
								<?php echo strip_tags( $additionalInfoCopy, '<br>' ); ?>
							</div>
						</article>
						<?php endif; ?>
					</div>
				</section>

				<section class="hours-schedule content-main">
					<div class="inner">
						<h2 class="section-title">Hours &amp; Schedule</h2>
						<?php foreach( $hoursScheduleItems as $hoursSchedule ) : ?>
						<article>
							<h3 class="entry-title"><?php echo $hoursSchedule['hours_schedule_heading'][1]; ?></h3>
							<div class="entry-content"><?php echo $hoursSchedule['hours_schedule_details'][1]; ?></div>
						</article>
						<?php endforeach; ?>
					</div>
				</section>

				<?php if( $groupSales ) : ?>
				<section class="event-specs banner">
					<ul class="slides">
<?php
						foreach( $groupSales as $groupSale ) :
							$post = get_post( $groupSale );
							setup_postdata($post);

							$bannerEvent = get_image('event_specs_image', 1, 1, 0);
							$postThumbID = hk_get_attachment_id_from_src( $bannerEvent );
							// $bannerEvent = wp_get_attachment_image( $postThumbID, 'banner-1600x800', false, array( 'class' => 'banner-image' ) );
							$headingMain = get('event_specs_heading_main');
							$alsoIncludedItems = get_field( 'event_specs_also_included' );
							$buttonLink = get('event_specs_button_link');
							$buttonText = get('event_specs_button_text');
							$modalLink = get('event_specs_modal_link');
?>
						<li class="slide" style="background: url(<?php echo $bannerEvent ?>)">
							<div class="page-meta">
								<header>
									<div class="header-meta">
										<p class="page-title"><?php the_title(); ?></p>
										<?php if( $headingMain ) { ?><h2 class="heading-main"><?php echo $headingMain ?></h2><?php } ?>
									</div>
								</header>

								<ul class="callouts">
									<li class="callout callout-1"><?php echo get('event_specs_icon_1') ?></li>
									<li class="callout callout-3"><?php echo get('event_specs_icon_3') ?></li>
								</ul>
								<ul class="callouts">
									<li class="callout callout-2"><?php echo get('event_specs_icon_2') ?></li>
									<li class="callout callout-4"><?php echo get('event_specs_icon_4') ?></li>
								</ul>

								<footer>
									<h3>Also Included</h3>
									<ul class="also-included">
										<?php foreach( $alsoIncludedItems as $alsoIncludedItem ) : ?>
										<li><?php echo $alsoIncludedItem ?></li>
										<?php endforeach; ?>
									</ul>
									<?php if( $modalLink && $modalLink !== 'none' ) : ?>
									<a class="button" href="#" data-modal="<?php echo $modalLink; ?>"><?php echo $buttonText ? $buttonText : 'Find Out More' ?></a>
									<?php elseif( $buttonLink ) : ?>
									<a class="button" href="<?php echo $buttonLink; ?>"><?php echo $buttonText ? $buttonText : 'Find Out More' ?></a>
									<?php endif; ?>
								</footer>
							</div><!-- .page-meta -->
						</li>
						<?php endforeach; ?>
					</ul>
				</section>
				<?php endif; ?>

				<?php if( $galleryImages ) : ?>
				<section class="gallery banner">
					<ul class="slides">
						<?php foreach( $galleryImages as $galleryImage ) : ?>
						<li class="slide">
							<img src="<?php echo $galleryImage['original'] ?>">
						</li>
						<?php endforeach; ?>
					</ul>
				</section>
				<?php endif; ?>

				<?php if( $postsList ) : ?>
				<section class="news-events content-main">
					<div class="inner">
						<h2 class="section-title">News &amp; Events</h2>
						<?php foreach( $postsList as $post ) : setup_postdata($post); ?>
						<article>
							<h3 class="entry-title"><a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__( 'Permalink to %s', 'boilerplate' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark"><?php the_title(); ?></a></h3>

							<div class="entry-content">

<?php
								if( $wordCount ) {
									hk_excerpt($wordCount);
									echo '<a href="' . get_permalink() . '" class="button read-more">Read More</a>';
								} else {
									the_excerpt();
								}
?>
								

								<?php edit_post_link( __( 'Edit', 'boilerplate' ), '', '' ); ?>
							</div><!-- .entry-content -->
						</article>
						<?php endforeach; ?>
					</div>
				</section>
				<?php endif; ?>

				<section class="sibling-list content-main">
					<div class="inner">
						<h2 class="section-title">Other Locations</h2>
						<ul class="siblings">
<?php
				$args = array(
					'posts_per_page'	=> -1,
					'post_type'			=> 'location',
					'orderby' 			=> 'menu_order',
					'order' 			=> 'ASC',
				);
				$subPages = get_posts( $args );
				foreach( $subPages as $subPage ) :
					$postThumbURL = get_image('page_options_thumbnail', 1, 1, 0, $subPage->ID);
					$postThumbID = hk_get_attachment_id_from_src( $postThumbURL );
					$postThumb = wp_get_attachment_image( $postThumbID, 'thumbnail-370x370' );
					if( $subPage->ID !== $currentID ) :
?>
							<li class="sibling">
								<?php echo $postThumb; ?>
								<a class="sibling-link" href="<?php echo get_permalink( $subPage->ID); ?>">
									<span class="title"><?php echo get_the_title( $subPage->ID ); ?></span>
									<span class="brief"><?php echo get('page_options_brief',1,1,$subPage->ID); ?></span>
								</a>
							</li>
					<?php endif; ?>
				<?php endforeach; ?>
						</ul>
					</div>
				</section>


<?php endwhile; ?>

<?php get_footer(); ?>