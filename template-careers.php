<?php
/**
 * Template name: Careers
 * Template for Careers page
 */

get_header();
?>

<?php
	if ( have_posts() ) while ( have_posts() ) :
		the_post();

		$mobileThumbURL = get('page_options_mobile_thumbnail');
		$mobileThumbID = hk_get_attachment_id_from_src($mobileThumbURL);
		$mobileThumbURL = wp_get_attachment_image_src( $mobileThumbID, $size='thumbnail-320x320' );
		$desktopThumbURL = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), $size='banner-1600x480' );
?>
				<?php if( $mobileThumbURL || $desktopThumbURL ) : ?>
				<section class="banner" data-small="<?php echo $mobileThumbURL[0]; ?>" data-large="<?php echo $desktopThumbURL[0]; ?>">
					<img class="banner-image" src="">
				</section>
				<?php endif; ?>

				<section class="content content-main">
					<div class="inner">

<?php
	// Get Sub-Pages
	$args = array(
		'posts_per_page'	=> -1,
		'post_type'			=> 'position',
		'orderby' 			=> 'menu_order',
		'order' 			=> 'ASC',
	);
	$positions = get_posts( $args );
?>
						<aside>
							<h1 class="entry-title">Current Positions</h1>
<?php if($positions ) : ?>
							<ul id="positions" class="positions">
<? foreach( $positions as $post ) : setup_postdata($post); ?>
								<li class="position" id="<?php echo $post->post_name; ?>">
									<h3 class="position-title"><?php echo $postTitle == "" ? get_the_title() : $postTitle; ?></h3>
									<div class="entry-content">
										<?php the_content(); ?>
										<span class="button" data-position="<?php echo $postTitle == "" ? get_the_title() : $postTitle; ?>">Apply Now</span>
										<?php edit_post_link( __( 'Edit', 'boilerplate' ), '', '' ); ?>
									</div>
								</li><!-- .position -->
<?php endforeach; ?>
							</ul><!-- #positions -->
<? else : ?>
							<ul class="xoxo">
								<li class="widget-container featured">
									<h3 class="position-title">There are no positions available right now.</h3>
								</li>
							</ul><!-- #positions -->
<?php endif; ?>
						</aside>

<?php wp_reset_postdata(); ?>
						<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
							<h1 class="entry-title"><?php the_title(); ?></h1>
							<div class="entry-content">
								<?php the_content(); ?>
								<?php edit_post_link( __( 'Edit', 'boilerplate' ), '', '' ); ?>
							</div><!-- .entry-content -->
						</article><!-- #post-## -->
<?php endwhile; ?>
					</div><!-- .inner -->
				</section>

<div class="modal" id="modal-application">
	<?php echo do_shortcode( '[contact-form-7 id="375" title="Careers"]' ) ?>
</div>
<?php get_footer(); ?>