<?php
/**
 * The template for displaying all pages.
 */

get_header(); ?>

<?php
	if ( have_posts() ) while ( have_posts() ) :
		the_post();

		$mobileThumbURL = get('page_options_mobile_thumbnail');
		$mobileThumbID = hk_get_attachment_id_from_src($mobileThumbURL);
		$mobileThumbURL = wp_get_attachment_image_src( $mobileThumbID, $size='thumbnail-320x320' );
		$desktopThumbURL = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), $size='banner-1600x550' );
?>
				<?php if( $mobileThumbURL || $desktopThumbURL ) : ?>
				<section class="banner" data-small="<?php echo $mobileThumbURL[0]; ?>" data-large="<?php echo $desktopThumbURL[0]; ?>">
					<img class="banner-image" src="">
				</section>
				<?php endif; ?>

				<section class="content content-main">
					<div class="inner">
						<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
							<h1 class="entry-title"><?php the_title(); ?></h1>
							<div class="entry-content">
								<?php the_content(); ?>
								<?php edit_post_link( __( 'Edit', 'boilerplate' ), '', '' ); ?>
							</div><!-- .entry-content -->
						</article><!-- #post-## -->

						<?php get_sidebar(); ?>
					</div>
				</section>
<?php endwhile; ?>

<?php get_footer(); ?>