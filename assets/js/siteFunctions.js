(function($) {
	var $window = $(window),
		didScroll = false,
		$html = $('html'),
		$body = $('body'),
		$skrim = $('#skrim'),
		$mainnav = $('#menu-primary'),
		$subnav = $('.subnav'),
		$mobilenav = $('#menu-mobile'),
		$placefull = $('#menu-placefull'),
		navPos,
		navHeight,
		$contentMain = $('.content-main');

	// Swap out no-js class
	$html.removeClass('no-js').addClass('js');

	// Swap banner image based on resolution
	function replaceBanners(){
		if ( 1190 < $( window ).width() ){
			$('[data-large]').each(function(){
				var $this = $(this),
					imgURL = $this.data('large');

				if( imgURL.length ){
					$this.find('.banner-image').attr('src', '').attr('src', imgURL);
				}
			});
		} else {

			$('[data-small]').each(function(){
				var $this = $(this),
					imgURL = $this.data('small');
				if( imgURL.length ){
					$this.find('.banner-image').attr('src', '').attr('src', $this.data('small'));
				}
			});
		}
	}
	replaceBanners();

	// Trigger link when clicking anywhere on widget
	$('#site-footer,.single-location aside').find('.vcard').click(function(e){
		var $target = $(this).find('a');
		e.preventDefault();

		if( $target.attr('data-modal') ){
			launchModal( $target.data('modal') );
		} else {
			window.location = $target.attr('href');
		}
		
	});
	$('.home .featured-item').click(function(e){
		e.preventDefault();
		window.location = $(this).find('a').attr('href');
	});

	// Instagram gallery on home page
	// jQuery.fn.spectragram.accessData = {
	//     accessToken: '3367677.bebceba.03e38fd6cd4f4d88aff4aab0dd0250ef',
	//     clientID: 'bebceba6346549fbabb2bf14f50b91a1'
	// };

	// $('#instagram-gallery').spectragram('getUserFeed',{
	//     query: 'combatpics',
	//     max: 20
	// });

	// Home page slider
	$('.slider').flexslider({
		'controlNav': 		false,
		'directionNav': 	false,
		'slideshowSpeed': 	7000,
		'animationSpeed': 	600
	});

	// Sales Detail page slider
	$('.testimonials').flexslider({
		'slideshowSpeed': 	7000,
		'animationSpeed': 	600
	});

	// Location Detail main slider
	$('.single-location .event-specs').flexslider({
		'directionNav': 	false,
		'slideshowSpeed': 	10000,
		'animationSpeed': 	600
	});

	// Gallery sliders
	$('.gallery.banner').flexslider({
		'animation': 'slide',
		'controlNav': 	false,
		'slideshowSpeed': 	10000,
		'animationSpeed': 	400
	});

	// Back to top link
	$('#back-to-top').click(function(e){
		e.preventDefault();
		$('html,body').stop().animate({scrollTop: 0 });
	});

	// Pages w/ subnav
	if( $subnav.length ){
		// Let assets fully load before getting nav position
		$window.load(function() {
			navPos = $subnav.position().top,
			navHeight = $subnav.outerHeight();
		});

		function stuckTop() { 
			// Stick subnav to top of viewport
			if($html.scrollTop() >= navPos || $body.scrollTop() >= navPos) { 
				$body.addClass('fixed-top'); 
			} else { 
				$body.removeClass('fixed-top'); 
			}
		}
		stuckTop();

		function stuckBottom() {
			var contentPos;
			if( $contentMain.length ){
				contentPos = $contentMain.position().top;
			} else {
				return;
			}

			if( ($window.height() + $html.scrollTop()) >= contentPos || ($window.height() + $body.scrollTop()) >= contentPos) { 
				$body.removeClass('fixed-bottom'); 
			} else {
				$body.addClass('fixed-bottom');
			}
		}
		// stuckBottom();

		$window.scroll(function(){
		    didScroll = true;
		});

		setInterval(function() {
		    if ( didScroll ) {
		        didScroll = false;

				stuckTop();
				// stuckBottom();
		    }
		}, 100);

		$subnav.find('a[data-bookmark]').click(function(e){
			e.preventDefault();
			var $this = $(this),
				$target = $('.'+$this.data('bookmark'));

			if( $target.length ){
				// if( $target.position().top < navPos || !$body.hasClass('fixed-top') ){
				// 	$('html,body').animate({scrollTop: $target.position().top});
				// } else {
					$('html,body').animate({scrollTop: $target.position().top-navHeight});
				// }
			}
		});
	}

	// Accordion sections
	$('#positions').find('.position-title').click(function(e){
		$(this).toggleClass('expanded').next('.entry-content').slideToggle();
	}).next('.entry-content').hide();
	// End Accordion sections

	// Careers page stuff
	if( $('#positions').length ){

		// Launch modal
		$contentMain.find('.button').click(function(e){
			e.preventDefault();
			var $this = $(this),
				jobPosition = $this.data('position');

			if( jobPosition !== undefined ){
				$('#your-position').val( jobPosition );
			}
			launchModal('application');

		});

		// Trigger File Upload field on Careers form and use filename as label
		$('#file-upload').attr('readonly', 'readonly').click(function(e){
			$('#your-resume').click();
			setInterval(function () {
				$('#file-upload').val($('#your-resume').val());
			}, 1);
	        return false;
		});

	}
	// End Careers page stuff

	// Modal stuff
	$('[data-modal]').click(function(e){
		e.preventDefault();

		var $this = $(this);
		
		launchModal( $this.data('modal') );
	});
	$('.gallery-item-list').find('.thumbnail').unbind('click').click(function(e){
		e.preventDefault();

		var $this = $(this);

		$('#modal-gallery').css('margin-left', -$this.data('width') / 2).css('margin-top', -$this.data('height') / 2).find('img').attr('src', $this.data('src'));
		launchModal( $this.data('modal'), true );
	})

	function launchModal(modalName, noScroll){
		var $modal = $( '#modal-' + modalName );
		noScroll = noScroll || false;

		if( $modal.length ){
			$modal.add($skrim).fadeIn();

			if( !noScroll ){
				$('html,body').stop().animate({scrollTop: 0 });
			}
		}
	}

	$mainnav.find('.contact').click(function(e){
		e.preventDefault();
		$('#modal-contact').add($skrim).fadeIn();
	}).siblings('.book-now').click(function(e){
		e.preventDefault();
		$('#modal-book-now').add($skrim).fadeIn();
	})

	$mobilenav.find('.contact').click(function(e){
		e.preventDefault();
		$body.removeClass('menu-expanded');
		launchModal( 'contact' );
	})

	$placefull.find('.request-quote a').click(function(e){
		e.preventDefault();
		$body.removeClass('menu-expanded');
		$(this).closest('.modal').fadeOut(function(){
			launchModal( 'quote-request' );
		});
	});

	// Close modal
	$('.modal').find('.cancel, .close').add($skrim).click(function(e){
		$('.modal').add($skrim).fadeOut();
	});
	// End Modal stuff

	// Form fixes
	$('#your-date,#event-date').attr('placeholder', $('#your-date').attr('title'));
	$('#your-interest').find('input[type="checkbox"]').last().change(function() {
		var $this = $(this);
	    if ( $this.is(':checked') ) {
	        $('#your-interest').find('input[type="checkbox"]').prop( "checked", true );
	    }
	});

	// Open external links in new window
	$('a').each(function() {
		if( !$(this).closest('.flex-control-nav').length ){
		   var a = new RegExp('/' + window.location.host + '/');
		   if(!a.test(this.href)) {
		       $(this).click(function(event) {
		           event.preventDefault();
		           event.stopPropagation();
		           window.open(this.href, '_blank');
		       });
		   }
		}
	});

	// Toggle menu visibility for mobile
	$('#menu-toggle').click(function(e){
		$body.toggleClass('menu-expanded');
	})

	// Google Analytics Events
	$('#access').find('.facebook, .twitter').find('a').click(function(e){
		_gaq.push(['_trackEvent', 'Social Icons (Header)', 'Click', $(this).attr('title')]);
	});

	// Track scrolling for book now cta on mobile
	var lastScrollTop = 0,
		$cta = $('#cta-book-now');
		
	$(window).scroll(function(e){

		var st = $(this).scrollTop();

		if( $cta.css('text-align') === 'center' ){
			if (st > lastScrollTop){
				$('#cta-book-now:visible').fadeOut();
			} else {
				$('#cta-book-now:hidden').fadeIn();
			}
		}

		lastScrollTop = st;
	});

	$window.resize(function(){
		replaceBanners();
	})
})(jQuery);
