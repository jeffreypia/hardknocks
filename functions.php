<?php
/**
 * Hard Knocks functions and definitions
 */

if ( ! isset( $content_width ) )
	$content_width = 870;

if ( ! function_exists( 'hk_setup' ) ):

	function hk_setup() {

		// This theme styles the visual editor with editor-style.css to match the theme style.
		add_editor_style();

		// Add default posts and comments RSS feed links to head
		add_theme_support( 'automatic-feed-links' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'primary' => __( 'Primary Navigation', 'boilerplate' ),
			'secondary' => __( 'Secondary Navigation', 'boilerplate' ),
			'tertiary' => __( 'Tertiary Navigation', 'boilerplate' ),
			'social' => __( 'Social Icons', 'boilerplate' ),
			'mobile' => __( 'Mobile', 'boilerplate' ),
			'placefull' => __( 'Placefull', 'boilerplate' ),
		) );
	}
endif;
add_action( 'after_setup_theme', 'hk_setup' );

if ( ! function_exists( 'hk_filter_wp_title' ) ) :

	function hk_filter_wp_title( $title, $separator ) {
		// Don't affect wp_title() calls in feeds.
		if ( is_feed() )
			return $title;

		// The $paged global variable contains the page number of a listing of posts.
		// The $page global variable contains the page number of a single post that is paged.
		// We'll display whichever one applies, if we're not looking at the first page.
		global $paged, $page;

		if ( is_search() ) {
			// If we're a search, let's start over:
			$title = sprintf( __( 'Search results for %s', 'boilerplate' ), '"' . get_search_query() . '"' );
			// Add a page number if we're on page 2 or more:
			if ( $paged >= 2 )
				$title .= " $separator " . sprintf( __( 'Page %s', 'boilerplate' ), $paged );
			// Add the site name to the end:
			$title .= " $separator " . get_bloginfo( 'name', 'display' );
			// We're done. Let's send the new title back to wp_title():
			return $title;
		}

		// Otherwise, let's start by adding the site name to the end:
		$title .= get_bloginfo( 'name', 'display' );

		// If we have a site description and we're on the home/front page, add the description:
		$site_description = get_bloginfo( 'description', 'display' );
		if ( $site_description && ( is_home() || is_front_page() ) )
			$title .= " $separator " . $site_description;

		// Add a page number if necessary:
		if ( $paged >= 2 || $page >= 2 )
			$title .= " $separator " . sprintf( __( 'Page %s', 'boilerplate' ), max( $paged, $page ) );

		// Return the new title to wp_title():
		return $title;
	}
endif;
add_filter( 'wp_title', 'hk_filter_wp_title', 10, 2 );

if ( ! function_exists( 'hk_page_menu_args' ) ) :
	/**
	 * Get our wp_nav_menu() fallback, wp_page_menu(), to show a home link.
	 */
	function hk_page_menu_args( $args ) {
		$args['show_home'] = true;
		return $args;
	}
endif;
add_filter( 'wp_page_menu_args', 'hk_page_menu_args' );

if ( ! function_exists( 'hk_excerpt_length' ) ) :
	/**
	* Sets the post excerpt length to 40 characters.
	*/
   function hk_excerpt_length( $length ) {
	   return 40;
   }
endif;
add_filter( 'excerpt_length', 'hk_excerpt_length' );

if ( ! function_exists( 'hk_continue_reading_link' ) ) :
	/**
	 * Returns a "Continue Reading" link for excerpts
	 */
	function hk_continue_reading_link() {
		return ' <a class="button read-more" href="'. get_permalink() . '">' . __( 'Read More', 'boilerplate' ) . '</a>';
	}
endif;

if ( ! function_exists( 'hk_auto_excerpt_more' ) ) :
	/**
	 * Replaces "[...]" (appended to automatically generated excerpts) with an ellipsis and boilerplate_continue_reading_link().
	 */
	function hk_auto_excerpt_more( $more ) {
		return ' &hellip;' . hk_continue_reading_link();
	}
endif;
add_filter( 'excerpt_more', 'hk_auto_excerpt_more' );

if ( ! function_exists( 'hk_custom_excerpt_more' ) ) :
	/**
	 * Adds a pretty "Continue Reading" link to custom post excerpts.
	 */
	function hk_custom_excerpt_more( $output ) {
		if ( has_excerpt() && ! is_attachment() ) {
			$output .= hk_continue_reading_link();
		}
		return $output;
	}
endif;
add_filter( 'get_the_excerpt', 'hk_custom_excerpt_more' );

if ( ! function_exists( 'hk_widgets_init' ) ) :
	/**
	 * Register widgetized areas, including two sidebars and four widget-ready columns in the footer.
	 */
	function hk_widgets_init() {
		register_sidebar( array(
			'name' => __( 'Primary Widget Area', 'boilerplate' ),
			'id' => 'primary-widget-area',
			'description' => __( 'Used on Blog Listing and Archive pages', 'boilerplate' ),
			'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
			'after_widget' => '</li>',
			'before_title' => '<h3 class="widget-title">',
			'after_title' => '</h3>',
		) );
		register_sidebar( array(
			'name' => __( 'Secondary Widget Area', 'boilerplate' ),
			'id' => 'secondary-widget-area',
			'description' => __( 'Used on Blog Detail page', 'boilerplate' ),
			'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
			'after_widget' => '</li>',
			'before_title' => '<h3 class="widget-title">',
			'after_title' => '</h3>',
		) );
		register_sidebar( array(
			'name' => __( 'Generic Page Widget Area', 'boilerplate' ),
			'id' => 'generic-page-widget-area',
			'description' => __( 'Used on generic pages', 'boilerplate' ),
			'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
			'after_widget' => '</li>',
			'before_title' => '<h3 class="widget-title">',
			'after_title' => '</h3>',
		) );
		register_sidebar( array(
			'name' => __( 'Global Footer Widget Area', 'boilerplate' ),
			'id' => 'global-footer-widget-area',
			'description' => __( 'Global footer widgets', 'boilerplate' ),
			'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
			'after_widget' => '</li>',
			'before_title' => '<h3 class="widget-title">',
			'after_title' => '</h3>',
		) );
		register_sidebar( array(
			'name' => __( 'Global Footer Locations Area', 'boilerplate' ),
			'id' => 'global-footer-locations-area',
			'description' => __( 'Global footer locations', 'boilerplate' ),
			'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
			'after_widget' => '</li>',
			'before_title' => '<h3 class="widget-title">',
			'after_title' => '</h3>',
		) );
		register_sidebar( array(
			'name' => __( 'Locations Detail Widget Area', 'boilerplate' ),
			'id' => 'locations-detail-widget-area',
			'description' => __( 'Widgets on Locations Detail pages', 'boilerplate' ),
			'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
			'after_widget' => '</li>',
			'before_title' => '<h3 class="widget-title">',
			'after_title' => '</h3>',
		) );
		register_sidebar( array(
			'name' => __( 'FAQ Page Widget Area', 'boilerplate' ),
			'id' => 'faq-page-widget-area',
			'description' => __( 'Widgets on FAQ pages', 'boilerplate' ),
			'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
			'after_widget' => '</li>',
			'before_title' => '<h3 class="widget-title">',
			'after_title' => '</h3>',
		) );
		// register_sidebar( array(
		// 	'name' => __( 'Fourth Footer Widget Area', 'boilerplate' ),
		// 	'id' => 'fourth-footer-widget-area',
		// 	'description' => __( 'The fourth footer widget area', 'boilerplate' ),
		// 	'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
		// 	'after_widget' => '</li>',
		// 	'before_title' => '<h3 class="widget-title">',
		// 	'after_title' => '</h3>',
		// ) );
	}
endif;
add_action( 'widgets_init', 'hk_widgets_init' );

if ( ! function_exists( 'boilerplate_posted_on' ) ) :
	/**
	 * Prints HTML with meta information for the current post—date/time and author.
	 */
	function boilerplate_posted_on() {
		// BP: slight modification to Twenty Ten function, converting single permalink to multi-archival link
		// Y = 2012
		// F = September
		// m = 01–12
		// j = 1–31
		// d = 01–31
		printf( __( '<span class="entry-date">%2$s %3$s</span>', 'boilerplate' ),
			// %1$s = container class
			'meta-prep meta-prep-author',
			// %2$s = month: /yyyy/mm/
			sprintf( '<a class="entry-month" href="%1$s" title="%2$s" rel="bookmark">%3$s</a>',
				home_url() . '/' . get_the_date( 'Y' ) . '/' . get_the_date( 'm' ) . '/',
				esc_attr( 'View Archives for ' . get_the_date( 'F' ) . ' ' . get_the_date( 'Y' ) ),
				get_the_date( 'M' )
			),
			// %3$s = day: /yyyy/mm/dd/
			sprintf( '<a class="entry-day" href="%1$s" title="%2$s" rel="bookmark">%3$s</a>',
				home_url() . '/' . get_the_date( 'Y' ) . '/' . get_the_date( 'm' ) . '/' . get_the_date( 'd' ) . '/',
				esc_attr( 'View Archives for ' . get_the_date( 'F' ) . ' ' . get_the_date( 'j' ) . ' ' . get_the_date( 'Y' ) ),
				get_the_date( 'd' )
			),
			// %4$s = year: /yyyy/
			sprintf( '<a href="%1$s" title="%2$s" rel="bookmark">%3$s</a>',
				home_url() . '/' . get_the_date( 'Y' ) . '/',
				esc_attr( 'View Archives for ' . get_the_date( 'Y' ) ),
				get_the_date( 'Y' )
			),
			// %5$s = author vcard
			sprintf( '<span class="author vcard"><a class="url fn n" href="%1$s" title="%2$s">%3$s</a></span>',
				get_author_posts_url( get_the_author_meta( 'ID' ) ),
				sprintf( esc_attr__( 'View all posts by %s', 'boilerplate' ), get_the_author() ),
				get_the_author()
			)
		);
	}
endif;

if ( ! function_exists( 'boilerplate_posted_in' ) ) :
	/**
	 * Prints HTML with meta information for the current post (category, tags and permalink).
	 */
	function boilerplate_posted_in() {
		// Retrieves tag list of current post, separated by commas.
		$tag_list = get_the_tag_list( '', ', ' );
		if ( $tag_list ) {
			$posted_in = __( 'This entry was posted in %1$s and tagged %2$s. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'boilerplate' );
		} elseif ( is_object_in_taxonomy( get_post_type(), 'category' ) ) {
			$posted_in = __( 'This entry was posted in %1$s. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'boilerplate' );
		} else {
			$posted_in = __( 'Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'boilerplate' );
		}
		// Prints the string, replacing the placeholders.
		printf(
			$posted_in,
			get_the_category_list( ', ' ),
			$tag_list,
			get_permalink(),
			the_title_attribute( 'echo=0' )
		);
	}
endif;

// remove version info from head and feeds (http://digwp.com/2009/07/remove-wordpress-version-number/)
if ( ! function_exists( 'hk_complete_version_removal' ) ) :
	function hk_complete_version_removal() {
		return '';
	}
endif;
add_filter('the_generator', 'hk_complete_version_removal');

// change Search Form input type from "text" to "search" and add placeholder text
function hk_search_form ( $form ) {
	$form = '<form role="search" method="get" id="searchform" action="' . home_url( '/' ) . '" >
	<div><label class="screen-reader-text" for="s">' . __('Search for:') . '</label>
	<input type="search" placeholder="Search for..." value="' . get_search_query() . '" name="s" id="s" />
	<input type="submit" id="searchsubmit" value="'. esc_attr__('Search') .'" />
	</div>
	</form>';
	return $form;
}
add_filter( 'get_search_form', 'hk_search_form' );

// Get page ID by slug
function hk_get_ID_by_slug($page_slug) {
    $page = get_page_by_path($page_slug);
    if ($page) {
        return $page->ID;
    } else {
        return null;
    }
}

// Enable Excerpt field on pages
if ( function_exists( 'add_post_type_support' ) ) :
	add_post_type_support('page', 'excerpt');
endif;

// Enable pagination
function hk_paginate() {
	global $wp_query, $wp_rewrite;
	$wp_query->query_vars['paged'] > 1 ? $current = $wp_query->query_vars['paged'] : $current = 1;
	
	$pagination = array(
		'base' => @add_query_arg('page','%#%'),
		'format' => '',
		'total' => $wp_query->max_num_pages,
		'current' => $current,
		'show_all' => true,
		'type' => 'list',
		'prev_next' => false,
		);
	
	if( $wp_rewrite->using_permalinks() )
		$pagination['base'] = user_trailingslashit( trailingslashit( remove_query_arg( 's', get_pagenum_link( 1 ) ) ) . 'page/%#%/', 'paged' );
	
	if( !empty($wp_query->query_vars['s']) )
		$pagination['add_args'] = array( 's' => get_query_var( 's' ) );
	
	$pager = paginate_links( $pagination );
	if( $pager ) :
		echo '		<section id="pager">';
		echo '			<div class="inner">';
		echo '				<div class="prev">';
								previous_posts_link('&laquo; Previous Entries');
		echo '				</div>';
		echo 				$pager;
		echo '				<div class="next">';
								next_posts_link('&laquo; Next Entries');
		echo '				</div>';
		echo '			</div>';
		echo '		</section>';
	endif;
}

// Get image attachment ID by URL
function hk_get_attachment_id_from_src ($image_src) {
	global $wpdb;
	$query = "SELECT ID FROM {$wpdb->posts} WHERE guid='$image_src'";
	$id = $wpdb->get_var($query);
	return $id;
}

// Customize body classes
function hk_post_name_in_body_class( $classes ){
	if( is_page() ) {
		global $post;
		$parentID = $post->post_parent;
		if( $parentID ) :
			$parent = get_page($post->post_parent);
			array_push( $classes, "section-{$parent->post_name}" );
		endif;
		array_push( $classes, "{$post->post_type}-{$post->post_name}" );
	}
	return $classes;
}
add_filter( 'body_class', 'hk_post_name_in_body_class' );

// Customize Excerpt
function hk_excerpt($excerpt_length = 55, $id = false, $echo = true) {
	  
    $text = '';
    
	  if($id) {
	  	$the_post = & get_post( $my_id = $id );
	  	$text = ($the_post->post_excerpt) ? $the_post->post_excerpt : $the_post->post_content;
	  } else {
	  	global $post;
	  	$text = ($post->post_excerpt) ? $post->post_excerpt : get_the_content('');
    }
	  
		$text = strip_shortcodes( $text );
		$text = apply_filters('the_content', $text);
		$text = str_replace(']]>', ']]&gt;', $text);
	  $text = strip_tags($text);
	
		$excerpt_more = ' ' . '[...]';
		$words = preg_split("/[\n\r\t ]+/", $text, $excerpt_length + 1, PREG_SPLIT_NO_EMPTY);
		if ( count($words) > $excerpt_length ) {
			array_pop($words);
			$text = implode(' ', $words);
			$text = $text . $excerpt_more;
		} else {
			$text = implode(' ', $words);
		}
	if($echo)
  echo apply_filters('the_content', $text);
	else
	return $text;
}

function hk_get_excerpt($excerpt_length = 55, $id = false, $echo = false) {
 return my_excerpt($excerpt_length, $id, $echo);
}

// Enqueue scripts the right way!
function hk_register_scripts() {
	// Use Google's CDN for jQuery
	wp_deregister_script('jquery');
	wp_register_script('jquery', "http" . ($_SERVER['SERVER_PORT'] == 443 ? "s" : "") . "://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js", false, null);

	// wp_deregister_script('jquery-ui');
	// wp_register_script('jquery-ui', 'http' . ($_SERVER['SERVER_PORT'] == 443 ? 's' : '') . '://ajax.googleapis.com/ajax/libs/jqueryui/1.8.6/jquery-ui.min.js', array('jquery'), '1.8.6');

	wp_enqueue_script('plugins', get_template_directory_uri() . '/assets/js/plugins.js', array('jquery'), false, true);
	wp_enqueue_script('siteFunctions', get_template_directory_uri() . '/assets/js/siteFunctions.js', array('plugins'), false, true);
}
if (!is_admin()) add_action("wp_enqueue_scripts", "hk_register_scripts", 11);

// add thumbnail support
if ( function_exists( 'add_theme_support' ) ) :
	add_theme_support( 'post-thumbnails' );
endif;

if ( function_exists( 'add_image_size' ) ) { 
	add_image_size( 'banner-1600x800', 1600, 800, true ); // Missions, Weapons, Arenas, Group Sales Detail page banner
	add_image_size( 'banner-1600x550', 1600, 550, true ); // Home page banner
	add_image_size( 'banner-1600x480', 1600, 480, true ); // Generic page banner
	add_image_size( 'banner-1600x350', 1600, 350, true ); //  Home page main CTA
	add_image_size( 'banner-1600x300', 1600, 300, true ); //  banner
	add_image_size( 'banner-1170x550', 1170, 550, true ); // Home page featured blog
	add_image_size( 'featured-image', 870, 500, true ); // Blog featured image
	add_image_size( 'thumbnail-800x450', 800, 450, true ); //  16:9 thumbnail (Location Detail page)
	add_image_size( 'thumbnail-570x390', 570, 390, true ); // Arena, Gallery thumbs
	add_image_size( 'thumbnail-370x370', 370, 370, true ); // Child page thumbs
	add_image_size( 'thumbnail-370x250', 370, 250, true ); // Weapon thumbs
	add_image_size( 'thumbnail-320x320', 320, 320, true ); // Mobile thumb
	add_image_size( 'thumbnail-270x150', 270, 150, true ); // Home page feature thumbs
	add_image_size( 'thumbnail-170x170', 170, 170, true ); // Mission badges
}


?>
