<?php
/**
 * The Footer widget areas.
 */
?>

<?php if( !is_404() && is_active_sidebar( 'global-footer-widget-area' ) ) : ?>
			<div class="promotions">
				<div class="inner">
					<ul class="xoxo">
						<?php dynamic_sidebar( 'global-footer-widget-area' ); ?>
					</ul>
				</div><!-- .inner -->
			</div><!-- .promotions -->
<?php endif; ?>

<?php if ( is_active_sidebar( 'global-footer-locations-area' ) ) : ?>
			<div class="locations">
				<div class="inner">
					<h2 class="widget-section-title">Locations</h2>
					<ul class="xoxo">
						<?php dynamic_sidebar( 'global-footer-locations-area' ); ?>
					</ul>
					<p>&larr; Swipe to see more locations &rarr;</p>
				</div><!-- .inner -->
			</div><!-- .locations -->
<?php endif; ?>
