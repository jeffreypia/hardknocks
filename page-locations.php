<?php
/**
 * The template for Locations page.
 */

get_header();
?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
				<section class="banner map" style="display: none">
<?php
					$googleMap = get('page_options_alternate_banner_content');
					if( $googleMap ) :
						echo $googleMap;
					else :
						the_post_thumbnail( 'banner-1600x550', array('class'=>'banner-image') );
					endif;
?>
				</section>

				<section class="banner map" id="map-canvas">
				</section>

				<section class="children-list content-main">
					<div class="inner">
						<h1 class="section-title"><?php the_title(); ?></h1>
						<ul class="children">
<?php
							$args = array(
								'posts_per_page'	=> -1,
								'post_type'			=> 'location',
								'orderby' 			=> 'menu_order',
								'order' 			=> 'ASC',
							);
							$subPages = get_posts( $args );
							foreach( $subPages as $subPage ) :
								$postThumbURL = get_image('page_options_thumbnail', 1, 1, 0, $subPage->ID);
								$postThumbID = hk_get_attachment_id_from_src( $postThumbURL );
								$postThumb = wp_get_attachment_image( $postThumbID, 'thumbnail-370x370' );
?>
							<li class="child">
								<?php echo $postThumb; ?>
								<a class="child-link" href="<?php echo get_permalink( $subPage->ID); ?>">
									<span class="title"><?php echo get_the_title( $subPage->ID ); ?></span>
									<span class="brief"><?php echo get('page_options_brief',1,1,$subPage->ID); ?></span>
								</a>
							</li>
							<?php endforeach; ?>
						</div>
					</ul>
				</section>
<?php endwhile; ?>

<pre>
</pre>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
<script type="text/javascript">

					
					var hk_locations = [
<?php
	foreach( $subPages as $subPage ) :
		$lat = get('map_options_lat', 1, 1, $subPage->ID);
		$lng = get('map_options_lng', 1, 1, $subPage->ID);
		if( $lat && $lng ) :
?>
							[<?php echo $lat; ?>,<?php echo $lng; ?>],
<?php
		endif;
	endforeach;
?>
						],
						hk_location = new google.maps.LatLng(hk_locations[0][0], hk_locations[0][1]),
						marker,
						map;
					function initialize() {
						var styles = [
								{
									"stylers": [
										{ "saturation": -100 }
									]
								},
								{
								}
							],
							mapOptions = {
								mapTypeControlOptions: {
									mapTypeIds: [ 'Styled']
								},
								zoom: 6,
								mapTypeId: 'Styled',
								draggable: false,
								scrollwheel: false, 
								disableDefaultUI: true
								// panControl: false,
								// zoomControl: false,
								// scaleControl: false,
								// streetViewControl: false,
								// disableDoubleClickZoom: true
							},
							bounds = new google.maps.LatLngBounds();
						map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

						var styledMapType = new google.maps.StyledMapType(styles, { name: 'Styled' });
						map.mapTypes.set('Styled', styledMapType);

						for (i = 0; i < hk_locations.length; i++) { 
							var letter = String.fromCharCode("A".charCodeAt(0) + (i));
							marker = new google.maps.Marker({
								map:map,
								draggable:false,
								animation: google.maps.Animation.DROP,
								icon: 'http://www.googlemapsmarkers.com/v1/'+letter+'/5ea939/FFFFFF/5ea939/',	
								position: new google.maps.LatLng(hk_locations[i][0], hk_locations[i][1])
							});

							//extend the bounds to include each marker's position
							bounds.extend(marker.position);

							google.maps.event.addListener(marker, 'click', toggleBounce);
						}
						map.fitBounds(bounds);

						google.maps.event.addDomListener(window, 'resize', function() {
							map.fitBounds(bounds);
						});
					}
					function toggleBounce() {
						if (marker.getAnimation() != null) {
							marker.setAnimation(null);
						} else {
							marker.setAnimation(google.maps.Animation.BOUNCE);
						}
					}
					google.maps.event.addDomListener(window, 'load', initialize);
</script>



<?php get_footer(); ?>