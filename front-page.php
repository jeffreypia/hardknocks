<?php
/**
 * The template for the home page.
 */

	get_header();

	$mobileThumbURL = get('page_options_mobile_thumbnail');
	$mobileThumbID = hk_get_attachment_id_from_src($mobileThumbURL);
	$mobileThumb = wp_get_attachment_image( $mobileThumbID, 'thumbnail-320x320' );

	$args = array(
		'posts_per_page'	=> 1,
		'post_type'			=> 'home_page',
	);
	$homePage = get_posts( $args );

	foreach( $homePage as $post ) : setup_postdata($post);
		$banners = get_group('banner');
?>
				<section class="banner slider <?php echo $mobileThumbID ?>">
		<?php if( $mobileThumb ) : ?>
					<div class="mobile-thumb">
						<?php echo $mobileThumb; ?>
					</div>
		<?php endif; ?>

		<?php if( $banners ) : ?>					
					<ul class="slides">
<?php
			foreach( $banners as $banner ) :
				$postThumbURL = $banner['banner_image'][1]['original'];
				$postThumbID = hk_get_attachment_id_from_src( $postThumbURL );
				$bannerImage = wp_get_attachment_image( $postThumbID, 'banner-1600x550', false, array( 'class' => 'banner-image' ) );
				$buttonLink = $banner['banner_button_link'][1];
				$buttonText = $banner['banner_button_text'][1];
?>
						<li class="slide">
							<?php echo $bannerImage; ?>
							<div class="page-meta">
								<p class="heading-main"><?php echo strip_tags( $banner['banner_heading_main'][1], '<br>' ); ?></p>
								<p class="heading-sub"><?php echo strip_tags( $banner['banner_heading_sub'][1], '<br>' ); ?></p>
								<?php if( $buttonLink ) : ?>
									<a class="button" href="<?php echo $buttonLink; ?>"><?php echo $buttonText ? $buttonText : 'Find Out More' ?></a>
								<?php endif; ?>
								<?php edit_post_link( __( 'Edit', 'boilerplate' ), '', '' ); ?>
							</div>
						</li>
			<?php endforeach; ?>
					</ul>
<?php
		else :
			the_post_thumbnail( 'banner-1600x550', array('class'=>'banner-image') );
		endif;
?>
				</section>

				<section class="featured-item-list">
					<div class="inner">
						<ul class="featured-items">
<?php
							$featuredItems = get_group( 'feature' );

							// Loop through Featured Items
							foreach( $featuredItems as $featuredItem ):
								$postThumbURL = $featuredItem['feature_thumbnail'][1]['original'];
								$postThumbID = hk_get_attachment_id_from_src( $postThumbURL );
								$postThumb = wp_get_attachment_image( $postThumbID, 'thumbnail-270x150' );
?>
							<li class="featured-item">
								<div class="thumbnail"><?php echo $postThumb; ?></div>
								<div class="entry-title"><?php echo $featuredItem['feature_title'][1]; ?></div>
								<div class="entry-content"><?php echo strip_tags( $featuredItem['feature_description'][1], '<br>' ); ?></div>
								<a class="featured-item-link" href="<?php echo $featuredItem['feature_link'][1] ?>"><?php echo $featuredItem['feature_title'][1]; ?></a>
							</li>
							<?php endforeach; ?>
						</ul>
					</div>
				</section>

				<section class="cta">
<?php
					$ctaTitle = strip_tags( get('call_to_action_heading_main', 1, 1), '<br>' );
					$ctaSubTitle = strip_tags( get('call_to_action_heading_sub', 1, 1), '<br>' );
					$ctaLink = get('call_to_action_button_link', 1, 1);
					$postThumbURL = get_image('call_to_action_image', 1, 1, 0);
					$postThumbID = hk_get_attachment_id_from_src( $postThumbURL );
					$postThumb = wp_get_attachment_image( $postThumbID, 'banner-1600x350', false, array( 'class' => 'banner-image' ) );
?>
					<?php if( $ctaLink ) { ?><a href="<?php echo $ctaLink ?>"><?php } ?>
						<?php echo $postThumb; ?>
					<?php if( $ctaLink ) { ?></a><?php } ?>
					<article>
						<?php if( $ctaTitle ) { ?><div class="entry-title"><?php echo $ctaTitle; ?></div><?php } ?>
						<?php if( $ctaSubTitle ) { ?><div class="entry-content"><?php echo $ctaSubTitle; ?></div><?php } ?>
					</article>
				</section>

				<section class="featured-post content-main">
<?php
					$args = array(
					   'posts_per_page' => 1,
					   'category_name' => 'featured',
					);
					$featuredPosts = get_posts ( $args );
					$featuredPost = $featuredPosts[0];
					$postThumbURL = get_image('featured_post_image', 1, 1, 0);
					$postThumbID = hk_get_attachment_id_from_src( $postThumbURL );
					$postThumb = wp_get_attachment_image( $postThumbID, 'banner-1170x550' );
?>
					<div class="inner">
						<h2 class="section-title">From Our Blog</h2>
						<div class="featured-image"><a href="<?php echo get_permalink($featuredPost->ID); ?>"><?php echo $postThumb; ?></a></div>
						<article>
							<div class="entry-title"><a href="<?php echo get_permalink($featuredPost->ID); ?>"><?php echo $featuredPost->post_title; ?></a></div>
							<div class="entry-content"><?php echo $featuredPost->post_excerpt; ?></div>
							<a href="<?php echo get_permalink($featuredPost->ID); ?>" class="button read-more">Read More</a>
							<ul class="menu share">
								<li class="menu-item facebook"><a id="facebook-share" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo get_permalink($featuredPost->ID); ?>" target="_blank">Share This</a></li>
								<li class="menu-item twitter"><a id="twitter-share" href="http://twitter.com/home?status=Currently reading <?php echo get_permalink($featuredPost->ID); ?> via @HardKnocks" title="Click to send this page to Twitter!" target="_blank">Tweet This</a></li>
								<!-- <li class="menu-item google"><a id="google-share" href="https://plus.google.com/share?url=<?php echo get_permalink($featuredPost->ID); ?>" target="_blank">Share This on Google+</a></li> -->
							</ul>
							<?php edit_post_link( __( 'Edit', 'boilerplate' ), '', '', $featuredPost->ID ); ?>
						</article>
					</div>
				</section>

				<section class="cta">
<?php
					$ctaTitle = strip_tags( get('call_to_action_heading_main', 2, 1), '<br>' );
					$ctaSubTitle = strip_tags( get('call_to_action_heading_sub', 2, 1), '<br>' );
					$ctaLink = get('call_to_action_button_link', 2, 1);
					$postThumbURL = get_image('call_to_action_image', 2, 1, 0);
					$postThumbID = hk_get_attachment_id_from_src( $postThumbURL );
					$postThumb = wp_get_attachment_image( $postThumbID, 'banner-1600x350', false, array( 'class' => 'banner-image' ) );
?>
					<?php if( $ctaLink ) { ?><a href="<?php echo $ctaLink ?>"><?php } ?>
						<?php echo $postThumb; ?>
					<?php if( $ctaLink ) { ?></a><?php } ?>
					<article>
						<?php if( $ctaTitle ) { ?><div class="entry-title"><?php echo $ctaTitle; ?></div><?php } ?>
						<?php if( $ctaSubTitle ) { ?><div class="entry-content"><?php echo $ctaSubTitle; ?></div><?php } ?>
					</article>
				</section>

				<section class="cta-footer">
<?php
	$button1Text = get('call_to_action_footer_button_copy_1');
	$button2Link = get('call_to_action_footer_button_link_2');
	$button2Text = get('call_to_action_footer_button_copy_2');
?>
					<div class="entry-title"><?php echo strip_tags( get('call_to_action_footer_heading'), '<br>' ); ?></div>
					<a href="<?php echo get('call_to_action_footer_button_link_1') ?>" class="button"><?php echo $button1Text ? $button1Text : 'Learn More'; ?></a>
					<?php if( $button2Link ) : ?>
					<a href="<?php echo $button2Link ?>" class="button"><?php echo $button2Text ? $button2Text : 'Learn More'; ?></a>
					<?php endif; ?>
				</section>
				
<?php endforeach; ?>

<?php get_footer(); ?>