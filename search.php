<?php
/**
 * The template for displaying Search Results pages.
 */

get_header(); ?>

				<section class="content content-main">
					<div class="inner">
<?php if ( have_posts() ) : ?>
						<h1 class="page-title"><?php printf( __( 'Search Results for: %s', 'boilerplate' ), '' . get_search_query() . '' ); ?></h1>
						<?php get_template_part( 'loop', 'search' ); ?>
<?php else : ?>
						<h2 class="page-title"><?php _e( 'Nothing Found', 'boilerplate' ); ?></h2>
						<article>
							<p><?php _e( 'Sorry, but nothing matched your search criteria. Please try again with some different keywords.', 'boilerplate' ); ?></p>
							<?php get_search_form(); ?>
						</article>
<?php endif; ?>
						<?php get_sidebar(); ?>
					</div><!-- .inner -->
				</section><!-- .content-main -->

				<?php hk_paginate() ?>

<?php get_footer(); ?>
