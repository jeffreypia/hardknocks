<?php
/**
 * Template name: Weapons
 * Template for Weapons page
 */

get_header();
$parentID = $post->post_parent;

	if ( have_posts() ) while ( have_posts() ) :
		the_post();

		$mobileThumbURL = get('page_options_mobile_thumbnail');
		$mobileThumbID = hk_get_attachment_id_from_src($mobileThumbURL);
		$mobileThumbURL = wp_get_attachment_image_src( $mobileThumbID, $size='thumbnail-320x320' );
		$desktopThumbURL = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), $size='banner-1600x550' );
?>
				<?php if( $mobileThumbURL || $desktopThumbURL ) : ?>
				<section class="banner" data-small="<?php echo $mobileThumbURL[0]; ?>" data-large="<?php echo $desktopThumbURL[0]; ?>">
					<img class="banner-image" src="">
					<div class="page-meta">
						<h1 class="page-title"><?php the_title(); ?></h1>
						<p class="heading-main"><?php echo strip_tags( get('page_options_banner_heading_main') ); ?></p>
						<p class="heading-sub"><?php echo strip_tags( get('page_options_banner_heading_sub'), '<br>' ); ?></p>
						<?php echo get('page_options_banner_copy'); ?>
						<?php edit_post_link( __( 'Edit', 'boilerplate' ), '', '' ); ?>
					</div>
				</section>
				<?php endif; ?>
<?php endwhile; ?>

				<nav class="subnav">
					<div class="inner">
						<ul class="menu">
							<li class="menu-item"><a href="#" data-bookmark="banner">Overview</a></li>
							<li class="menu-item"><a href="#" data-bookmark="content-main"><?php the_title(); ?></a></li>
							<li class="menu-item"><a href="#" data-modal="share">Share</a></li>
							<li class="menu-item book-now"><a href="#" data-modal="book-now">Book Now</a></li>
						</ul>
					</div>
				</nav>

				<section class="weapon-list content-main">
					<div class="inner">
						<h2 class="section-title">Weapons</h2>
						<ul class="weapons">
<?php
	$args = array(
		'posts_per_page' 	=> -1,
		'post_type' 		=> 'weapon',
		'orderby' 			=> 'menu_order',
		'order' 			=> 'ASC',
	);
	$weapons = get_posts( $args ); 

	// Loop through weapons
	foreach( $weapons as $weapon ):
?>
							<li class="weapon">
								<div class="thumbnail"><?php echo get_the_post_thumbnail($weapon->ID, 'thumbnail-570x390'); ?></div>
								<div class="title"><?php echo get_the_title( $weapon->ID ); ?></div>
								<div class="description"><?php echo $weapon->post_content; ?></div>
								<?php edit_post_link( __( 'Edit', 'boilerplate' ), '', '', $weapon->ID ); ?>
							</li>
<?php
	endforeach;
?>
						</ul>
					</div>
				</section>

				<section class="sibling-list content-sub">
					<div class="inner">
						<h2 class="section-title"><?php echo get_the_title($parentID); ?></h2>
						<ul class="siblings">
<?php
							$args = array(
								'posts_per_page'	=> -1,
								'post_type'			=> 'page',
								'post_parent'		=> $parentID,
								'orderby' 			=> 'menu_order',
								'order' 			=> 'ASC',
							);
							$subPages = get_posts( $args );
							global $post;
							foreach( $subPages as $subPage ) :
								$postThumbURL = get_image('page_options_thumbnail', 1, 1, 0, $subPage->ID);
								$postThumbID = hk_get_attachment_id_from_src( $postThumbURL );
								$postThumb = wp_get_attachment_image( $postThumbID, 'thumbnail-370x370' );
?>
							<li class="sibling">
								<?php echo $postThumb; ?>
								<a class="sibling-link" href="<?php echo get_permalink( $subPage->ID); ?>">
									<span class="title"><?php echo get_the_title( $subPage->ID ); ?></span>
								</a>
							</li>
							<?php endforeach; ?>
						</div>
					</ul>
				</section>

<?php get_footer(); ?>