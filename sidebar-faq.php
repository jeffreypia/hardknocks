<?php
/**
 * The Sidebar containing the primary and secondary widget areas.
 */
?>
		<aside>
			<ul class="xoxo">
<?php
			if ( is_active_sidebar( 'faq-page-widget-area' ) ) : 
				dynamic_sidebar( 'faq-page-widget-area' );
			endif;

			if ( is_active_sidebar( 'generic-page-widget-area' ) ) : 
				dynamic_sidebar( 'generic-page-widget-area' );
			endif;
?>
			</ul>
		</aside>
