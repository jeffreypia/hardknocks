<?php
/**
 * The main template file.
 */

get_header(); ?>

				<section class="content content-main">
					<div class="inner">
						<h1 class="page-title"><?php echo apply_filters('the_title',get_page( get_option('page_for_posts') )->post_title); ?></h1>
						<?php get_template_part( 'loop', 'index' ); ?>

						<?php get_sidebar(); ?>
					</div><!-- .inner -->
				</section><!-- .content-main -->

				<?php hk_paginate() ?>

<?php get_footer(); ?>
