<?php
/**
 * The template for displaying the footer.
 */
?>

<?php if( !is_front_page() ) : ?>
		<a id="back-to-top" href="#access">Back to top</a>
<?php endif; ?>

		<footer id="site-footer" class="site-footer" role="contentinfo">
			<?php get_sidebar( 'footer' ); ?>

			<div class="site-info">
				<div class="inner">
					<h4><?php bloginfo( 'name' ); ?></h4>
					<nav role="navigation" class="menu-footer">
						<?php wp_nav_menu( array( 'menu' => 'secondary', 'container' => '' ) ); ?>
					</nav>

					<?php wp_nav_menu( array( 'menu' => 'social', 'container_class' => 'menu-social' ) ); ?>
				</div>
			</div><!-- .site-info -->

			<div class="footer-meta">
				<div class="inner">
					<span class="copyright">&copy; <?php echo date('Y'); ?> <a href="<?php echo home_url( '/' ) ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?>, indoorwar.com</a></span>
					<?php wp_nav_menu( array( 'menu' => 'tertiary', 'container' => '' ) ); ?>
				</div><!-- #footer-meta -->
			</div><!-- .inner -->
		</footer><!-- footer -->

		<div id="skrim"></div>
		<div class="modal" id="modal-contact"><?php echo do_shortcode( '[contact-form-7 id="382" title="Contact"]' ); ?></div>
		<div class="modal" id="modal-book-now">
			<?php wp_nav_menu( array( 'menu' => 'placefull', 'container' => '' ) ); ?>
			<button class="close">Close</button>
		</div>



		<div class="modal" id="modal-newsletter">
			<form method="post" action="https://app.icontact.com/icp/signup.php" name="icpsignup" id="icpsignup13471" accept-charset="UTF-8" onsubmit="return verifyRequired13471();" >
				<input type="hidden" name="redirect" value="http://www.indoorwar.com/thank-you/">
				<input type="hidden" name="errorredirect" value="http://www.icontact.com/www/signup/error.html">
			    <input type="hidden" name="listid" value="173052">
			    <input type="hidden" name="specialid:173052" value="P4QL">
			    <input type="hidden" name="clientid" value="346508">
			    <input type="hidden" name="formid" value="13471">
			    <input type="hidden" name="reallistid" value="1">
			    <input type="hidden" name="doubleopt" value="0">

				<h3>Get Our Newsletter</h3>
				<p class="input-wrapper">
					<span class="wpcf7-form-control-wrap your-email"><input type="email" placeholder="Email" aria-required="true" id="your-email" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" size="40" value="" name="fields_email"></span> </p>
				<p class="input-wrapper">
					<span class="button-action"><input type="submit" class="wpcf7-form-control wpcf7-submit" value="Signup"><img class="ajax-loader" src="http://www.indoorwar.com/wp-content/plugins/contact-form-7/images/ajax-loader.gif" alt="Sending ..." style="visibility: hidden;"></span> <span class="button-alt cancel">Cancel</span></p>
			</form>
		</div>
		<div class="modal" id="modal-quote-request"><?php echo do_shortcode( '[contact-form-7 id="1445" title="Request Quote"]' ); ?></div>
		<div class="modal" id="modal-newsletter-2">
			<!-- Begin newsletter signup -->
			<div class="newsletter"> <img src="http://www.indoorwar.com/wp-content/themes/hko/images/ftr-title-newsletter.gif" height="20" alt="Newsletter Signup" class="title" />
				<div class="box">
				    <script type="text/javascript">
				    var icpForm13471 = document.getElementById('icpsignup13471');
				    if (document.location.protocol === "https:")
				        icpForm13471.action = "https://app.icontact.com/icp/signup.php";

				    function verifyRequired13471() {
				        if (icpForm13471["fields_email"].value == "") {
				            icpForm13471["fields_email"].focus();
				            alert("The Email field is required.");
				            return false;
				        }
				        return true;
				    }
				    </script>

				    <form method="post" action="https://app.icontact.com/icp/signup.php" name="icpsignup" id="icpsignup13471" accept-charset="UTF-8" onsubmit="return verifyRequired13471();" >
				        <input type="hidden" name="redirect" value="http://www.indoorwar.com/newsletter/">
				        <input type="hidden" name="errorredirect" value="http://www.icontact.com/www/signup/error.html">
				        <input type="hidden" name="listid" value="173052">
				        <input type="hidden" name="specialid:173052" value="P4QL">
				        <input type="hidden" name="clientid" value="346508">
				        <input type="hidden" name="formid" value="13471">
				        <input type="hidden" name="reallistid" value="1">
				        <input type="hidden" name="doubleopt" value="0">
				        <input type="text" name="fields_email" width="25">
				        <br />
				        <br />
				        <input type="image" src="http://www.indoorwar.com/wp-content/themes/hko/images/ftr-newsletter-submit.png" value="Submit" />
				    </form>
				    <!--  End newsletter signup -->    
				</div>
			</div>
		</div>


		<div class="modal" id="modal-share">
			<h3>Share this with your friends!</h3>
			<span class='st_facebook_large' displayText='Tweet'></span>
			<span class='st_twitter_large' displayText='Tweet'></span>
			<span class='st_linkedin_large' displayText='LinkedIn'></span>
			<span class='st_pinterest_large' displayText='Pinterest'></span>
			<span class='st_email_large' displayText='Email'></span>
			<span class='st_googleplus_large' displayText='Google +'></span>
			<span class='st_sharethis_large' displayText='ShareThis'></span>			
		</div>
<?php /* Share This */ ?>
<script type="text/javascript">var switchTo5x=false;</script>
<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
<script type="text/javascript">stLight.options({doNotHash:true, 
doNotCopy:true,hashAddressBar:false});</script>
<?php wp_footer(); ?>
<script type="text/javascript">
adroll_adv_id = "SA3PMLH2MREUXGWZ5ENNTG";
adroll_pix_id = "ESNSTZTGMBCNXNAN23ITKI";
(function () {
var oldonload = window.onload;
window.onload = function(){
   __adroll_loaded=true;
   var scr = document.createElement("script");
   var host = (("https:" == document.location.protocol) ? "https://s.adroll.com" : "http://a.adroll.com");
   scr.setAttribute('async', 'true');
   scr.type = "text/javascript";
   scr.src = host + "/j/roundtrip.js";
   ((document.getElementsByTagName('head') || [null])[0] ||
    document.getElementsByTagName('script')[0].parentNode).appendChild(scr);
   if(oldonload){oldonload()}};
}());
</script>
<!-- Google Code for Remarketing Tag -->
<!--------------------------------------------------
Remarketing tags may not be associated with personally identifiable
information or placed on pages related to sensitive categories. See more
information and instructions on how to setup the tag on:
http://google.com/ads/remarketingsetup
--------------------------------------------------->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 999091096;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript"
src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt=""
src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/999091096/?v
alue=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
	</body>
</html>