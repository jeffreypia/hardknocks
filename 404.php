<?php
/**
 * The template for displaying 404 pages (Not Found).
 */

get_header(); ?>

				<section class="content content-main">
					<div class="inner">
						<article id="post-0" class="post error404 not-found" role="main">
							<h1><?php _e( 'Not Found', 'boilerplate' ); ?></h1>
							<p><?php _e( 'Apologies, but the page you requested could not be found. Perhaps searching will help.', 'boilerplate' ); ?></p>
							<?php
								get_search_form();
								// add focus to search <input>
								echo '<script>document.getElementById(\'s\') && document.getElementById(\'s\').focus();</script>'.PHP_EOL;
							?>
						</article>

						<?php get_sidebar(); ?>
					</div><!-- .inner -->
				</section><!-- .content-main -->

<?php get_footer(); ?>
