<?php
/**
 * The Sidebar containing the primary and secondary widget areas.
 */
?>
		<aside>
			<ul class="xoxo">
<?php
			if ( is_page() && is_active_sidebar( 'generic-page-widget-area' ) ) : 
				dynamic_sidebar( 'generic-page-widget-area' );
			elseif ( is_single() && is_active_sidebar( 'secondary-widget-area' ) ) : 
				dynamic_sidebar( 'secondary-widget-area' );
			elseif ( is_active_sidebar( 'primary-widget-area' ) ) : 
				dynamic_sidebar( 'primary-widget-area' );
			endif;
?>
			</ul>
		</aside>
